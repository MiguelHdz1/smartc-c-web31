'use strict';
module.exports = (sequelize, DataTypes) => {
  var requests = sequelize.define('requests', {
    status: DataTypes.CHAR,
    json_information: DataTypes.STRING,
    hash: DataTypes.STRING,
    id_action: DataTypes.INTEGER
  }, {});
  requests.associate = function(models) {
    // associations can be defined here
  };
  return requests;
};
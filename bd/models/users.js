'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('Users', {
    id:{type:DataTypes.STRING, primaryKey:true},
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.CHAR
  }, {});
  User.associate = function(models) {
   User.hasOne(models.Profile,{foreignKey:'user_id'});
   User.belongsToMany(models.contracts,{
       through: 'participants',
       foreignKey:'id_user'
   });
  };
  return User;
};
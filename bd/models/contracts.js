'use strict';
module.exports = (sequelize, DataTypes) => {
    var contracts = sequelize.define('contracts', {
        name: DataTypes.STRING,
        abi: DataTypes.STRING,
        byteCode: DataTypes.STRING,
        hash: DataTypes.STRING,
        route: DataTypes.STRING,
        user_id: DataTypes.STRING,
        status: DataTypes.CHAR
    }, {});
    contracts.associate = function (models) {
        contracts.belongsToMany(models.Users, {
            through: 'participants',
            foreignKey: 'id_contract'
        });
        //contracts.hasMany(models.participants, {foreignKey: 'id_contract'});
    };
    return contracts;
};
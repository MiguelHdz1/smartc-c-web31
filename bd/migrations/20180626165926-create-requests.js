'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('requests', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      status: {
        type: Sequelize.CHAR(1),
        allowNull: false
      },
      json_information: {
        type: Sequelize.STRING(300),
        allowNull: false
      },
      hash:{
          type: Sequelize.STRING(100),
          allowNull: false
      },
      id_action: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
            model: 'actions',
            key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('requests');
  }
};
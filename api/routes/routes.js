'use strict';

module.exports = function (app,io) {
    var controllers = require('../controllers/mainController');        
    var contractController = require('../controllers/contracts/contractsController');
    var contractMethods = require('../controllers/contracts/contractMethods');
    var contractOwnerMethods = require('../controllers/contracts/contractOwnerMethods');
    var lastWillMethods = require('../controllers/contracts/lastWill/lastWillMethods');
    var paticipantWillMethods = require('../controllers/contracts/lastWill/participantMethods');
    var marriage = require('../controllers/contracts/marriage/index');
    var marriageDeploy = require('../controllers/contracts/marriage/deploy');
    var marriageDeployv2 = require('../controllers/contracts/marriage/v2/deploy');
    var marriageMethods = require('../controllers/contracts/marriage/participantMethods');
    var marriageOwner = require('../controllers/contracts/marriage/ownerMethods');
    var lastWillv2 = require('../controllers/contracts/lastWill/v2/lastWillMethods');
    var lastWillv2Deploy = require('../controllers/contracts/lastWill/v2/deploy');
    var lastWillv2Methods = require('../controllers/contracts/lastWill/v2/participantMethods');
    var lastWillv2OwnMethods = require('../controllers/contracts/lastWill/v2/lastWillMethods');
    

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/login');
    }
    
    app.route('/create-user').get(controllers.formUser);
    app.route('/create-user').post(controllers.createUser);
    app.route('/get-balance').get(isLoggedIn,controllers.checkBalance);
    

    //////////////////////MANAGE CONTRACTS ////////////////////////
    app.route('/').get(isLoggedIn, contractController.showTemplates);
    app.route('/manage-contracts').get(isLoggedIn, contractController.manageContracts);
    app.route('/manage-contracts/create').get(isLoggedIn, contractController.createContract);
    app.route('/contracts/participant').get(isLoggedIn, contractController.showParticipants);
    app.route('/deploy-contract').post(contractController.deployContract);
    
    

    app.route('/contracts/owner').post(contractMethods.owner);
    app.route('/contracts/conditions').post(contractMethods.conditions);
    app.route('/contracts/product').post(contractMethods.product);
    app.route('/contracts/description').post(contractMethods.description);
    app.route('/contracts/price').post(contractMethods.price);
    app.route('/contracts/counter').post(contractMethods.counters);
    app.route('/contracts/percentage').post(contractMethods.percentage);
    app.route('/contracts/payment').post(contractMethods.makePayment);
    
    
    app.route('/contracts/:id/:type/belong').get(isLoggedIn,contractOwnerMethods.contractInfo);      
    app.route('/contracts/belong/owner').post(contractOwnerMethods.owner);
    app.route('/contracts/belong/conditions').post(contractOwnerMethods.conditions);
    app.route('/contracts/belong/product').post(contractOwnerMethods.product);
    app.route('/contracts/belong/description').post(contractOwnerMethods.description);
    app.route('/contracts/belong/price').post(contractOwnerMethods.price);
    app.route('/contracts/belong/counter').post(contractOwnerMethods.counters);
    app.route('/contracts/belong/percentage').post(contractOwnerMethods.percentage);    

    /////////////////////////// LAST WILL ////////////////////////////////////
    app.route('/manage-contracts/last-will/create').get(isLoggedIn,lastWillMethods.create);
    app.route('/manage-contracts/last-will/deploy').post(isLoggedIn,lastWillMethods.deploy);
    app.route('/contracts/last-will/owner').post(isLoggedIn,lastWillMethods.owner);
    app.route('/contracts/last-will/conditions').post(isLoggedIn,lastWillMethods.conditions);    
    app.route('/contracts/last-will/amount').post(isLoggedIn,lastWillMethods.amount);
    app.route('/contracts/last-will/status').post(isLoggedIn,lastWillMethods.lifeStatus);
    app.route('/contracts/last-will/counter').post(isLoggedIn,lastWillMethods.children);
    
    app.route('/contracts/:id/:type/participant').get(isLoggedIn,contractMethods.contractInfo(io));
    
    app.route('/contracts/last-will/owner/participant').post(isLoggedIn,paticipantWillMethods.owner);
    app.route('/contracts/last-will/conditions/participant').post(isLoggedIn,paticipantWillMethods.conditions);    
    app.route('/contracts/last-will/amount/participant').post(isLoggedIn,paticipantWillMethods.amount);
    app.route('/contracts/last-will/status/participant').post(isLoggedIn,paticipantWillMethods.lifeStatus);
    app.route('/contracts/last-will/counter/participant').post(isLoggedIn,paticipantWillMethods.children);
    app.route('/contracts/last-will/still-alive/participant').post(isLoggedIn,paticipantWillMethods.confirmLife);
    app.route('/contracts/last-will/fund/participant').post(isLoggedIn,paticipantWillMethods.fund);
    app.route('/contracts/last-will/sync/participant').post(isLoggedIn,paticipantWillMethods.askIfDead);
    app.route('/contracts/last-will/last-touch/participant').post(isLoggedIn,paticipantWillMethods.lastTouch);
    
    
    /////////////////////////////////// MARRIAGE ///////////////////////////////////////
    app.route('/contracts/marriage/create').get(isLoggedIn,marriage.create);
    app.route('/contracts/marriage-v2/create').get(isLoggedIn,marriage.v2create);
    app.route('/contracts/marriage/deploy').post(isLoggedIn,marriageDeploy.deployContract);
    app.route('/contracts/marriage/v2/deploy').post(isLoggedIn,marriageDeployv2.deployContract);
    
    app.route('/contracts/marriage/proposer/participant').post(isLoggedIn,marriageMethods.proposer);
    app.route('/contracts/marriage/balance/participant').post(isLoggedIn,marriageMethods.balance);
    app.route('/contracts/marriage/regimen/participant').post(isLoggedIn,marriageMethods.regimen);
    app.route('/contracts/marriage/proposed/participant').post(isLoggedIn,marriageMethods.proposed);
    app.route('/contracts/marriage/conditions/participant').post(isLoggedIn,marriageMethods.agreements);
    app.route('/contracts/marriage/proposed-date/participant').post(isLoggedIn,marriageMethods.proposalDate);
    app.route('/contracts/marriage/divorce').post(isLoggedIn,marriageMethods.endedDate);
    
    app.route('/contracts/marriage/answer-date/participant').post(isLoggedIn,marriageMethods.answeredDate);
    app.route('/contracts/marriage/divorce/participant').post(isLoggedIn,marriageMethods.ended);    
    app.route('/contracts/marriage/answer').post(isLoggedIn,marriageMethods.answerMarriage);
    app.route('/contracts/marriage/divorce-req').post(isLoggedIn,marriageMethods.askForDivorce);
    app.route('/contracts/marriage/divorce-answer').post(isLoggedIn,marriageMethods.answerForDivorce);
    app.route('/contracts/marriage/divorce-cancel').post(isLoggedIn,marriageMethods.cancelDivorce);
    app.route('/contracts/marriage/deposit').post(isLoggedIn,marriageMethods.deposit);
    
    app.route('/contracts/marriage/proposer/owner').post(isLoggedIn,marriageOwner.proposer);
    app.route('/contracts/marriage/proposed/owner').post(isLoggedIn,marriageOwner.proposed);
    app.route('/contracts/marriage/conditions/owner').post(isLoggedIn,marriageOwner.agreements);
    app.route('/contracts/marriage/proposed-date/owner').post(isLoggedIn,marriageOwner.proposalDate);
    app.route('/contracts/marriage/answer-date/owner').post(isLoggedIn,marriageOwner.answeredDate);    
    app.route('/contracts/marriage/divorce/owner').post(isLoggedIn,marriageOwner.endedDate);
    app.route('/contracts/marriage/balance/owner').post(isLoggedIn,marriageOwner.balance);
    app.route('/contracts/marriage/regimen/owner').post(isLoggedIn,marriageOwner.regimen);
    
    //Last will v2    
    app.route('/manage-contracts/last-will-v2/create').get(isLoggedIn,lastWillv2.create);
    app.route('/manage-contracts/last-will/deploy/v2').post(isLoggedIn,lastWillv2Deploy.deploy);
    
    app.route('/contracts/last-will/v2/owner/belong').post(isLoggedIn,lastWillv2OwnMethods.owner);
    app.route('/contracts/last-will/v2/conditions/belong').post(isLoggedIn,lastWillv2OwnMethods.conditions);    
    app.route('/contracts/last-will/v2/amount/belong').post(isLoggedIn,lastWillv2OwnMethods.amount);
    app.route('/contracts/last-will/v2/status/belong').post(isLoggedIn,lastWillv2OwnMethods.lifeStatus);
    app.route('/contracts/last-will/v2/counter/belong').post(isLoggedIn,lastWillv2OwnMethods.children);
    app.route('/contracts/last-will/v2/last-touch/belong').post(isLoggedIn,lastWillv2OwnMethods.lastTouch);
    
    app.route('/contracts/last-will/v2/owner/participant').post(isLoggedIn,lastWillv2Methods.owner);
    app.route('/contracts/last-will/v2/conditions/participant').post(isLoggedIn,lastWillv2Methods.conditions);    
    app.route('/contracts/last-will/v2/amount/participant').post(isLoggedIn,lastWillv2Methods.amount);
    app.route('/contracts/last-will/v2/status/participant').post(isLoggedIn,lastWillv2Methods.lifeStatus);
    app.route('/contracts/last-will/v2/counter/participant').post(isLoggedIn,lastWillv2Methods.children);    
    app.route('/contracts/last-will/v2/last-touch/participant').post(isLoggedIn,lastWillv2Methods.lastTouch);
    
    app.use(function (req, res) {
        res.status(404).render('layouts/error');
    });
};
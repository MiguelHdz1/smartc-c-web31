module.exports = function (app, passport) {

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/signin');
    }

    function eraseCache(req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
    }

    var controllers = require('../controllers/mainController');

    app.route('/logout').get(controllers.logout,eraseCache);
    app.route('/login').get(controllers.signin);

    app.post('/login', passport.authenticate('local-signin', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true,
        successFlash: true,
        badRequestMessage: 'Ingresar todo los datos'
    }

    ));
};

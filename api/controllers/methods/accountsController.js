'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');

module.exports = {


    unlockAccount: function (account, password) {
        var result = false;        
            methods.web3.eth.defaultAccount = account;
            return methods.web3.eth.personal.unlockAccount(methods.web3.eth.defaultAccount, password);
    },
    newAccount: function (password, confirmPassword) {
        let message = '';
        let result = false;
        let account = '';
        if (password.length >= 5 && password.length <= 10) {
            if (password === confirmPassword && password !== '') {
                result = true;
                message = 'Cuenta creada correctamente';
                account = methods.web3.eth.personal.newAccount(password);
            } else {
                message = 'Verifique que ambas contraseñas sean iguales';
                account = null;
            }
        } else {
            message = "La contraseña debe ser mayor a 4 y menor a 11 caracteres";
            account = null;
        }
        return {'status': result, 'message': message, 'account': account};
    }
};
'use strict';
var accounts = require('./methods/accountsController');
var models = require('./../../bd/models');
var bcrypt = require('bcrypt-nodejs');
var methods = require('../conf/conf');


exports.signin = function (req, res) {
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
    if (!req.user) {
        res.render('js/signin', {error: req.flash('error')});
    } else {
        return res.redirect('/');
    }
};

exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.redirect('/');

    });    
};

exports.formUser = function (req, res) {
    if (!req.user) {
        res.render('createUser');
    } else {
        res.redirect('/');
    }
};    

exports.checkBalance= function(req,res){
    methods.web3.eth.getBalance(req.user.id, function (err, balance) {
     if(!err){
       res.status(202).send({'status': true, 'result': (balance/1000000000000000000)});  
     }else{
         res.status(202).send({'status': false, 'message': 'Ocurrio un error intente de nuevo'});
     }
    });
};

exports.createUser = function (req, res) {
    res.header("Content-Type", "application/json");
    res.header("Access-Control-Allow-Origin", '*');
    //console.log(req.body.password_user);
    models.Users.findOne({where: {email: req.body.email}}).then(function (users) {
        if (users) {
            res.status(202).send({'status': false, 'message': 'El usuario ya se encuentra registrado'});
        } else {
            var address = accounts.newAccount(req.body.password_user, req.body.password_confirm);
            if (address.status) {
                address.account.then(function(account){                    
                    models.Users.create({
                    id: account,
                    email: req.body.email,
                    password: bcrypt.hashSync(req.body.password_user, bcrypt.genSaltSync(8), null),                    
                    status: '1'
                }).then(function (result) {
                    models.Profile.create({
                        name: req.body.name_user,
                        first_name: req.body.first_name,
                        last_name: req.body.last_name,
                        user_id: result.id
                    }).then(function (result) {
                        res.status(202).send({'status': true, 'message': 'Cuenta creada correctamente'});
                    }).catch(function (err) {
                        res.status(500).send({'message': 'El perfil no se ha podido agregar'});
                    });
                }).catch(function (err) {
                    res.status(500).send({'message': 'El usuario no se ha podido agregar'});
                });
                }).catch(function(err){
                    res.status(500).send({'message': 'El usuario no se ha podido agregar'});
                });
                
            } else {
                res.status(500).send({'message': 'El usuario no se ha podido agregar'});
            }
        }
    });
};
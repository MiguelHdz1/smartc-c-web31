'use strict';
var models = require('../../../bd/models');
var methods = require('../../conf/conf');
var account = require('../methods/accountsController');
var op = require('sequelize').Op;
var marriageSocket = require('../sockets/marriage/marriageSocket');
var sellSocket = require('../sockets/sell/sellSocket');
var lastWillSocket = require('../sockets/lastwill/lastWillSocket');
module.exports = {

    contractInfo: function (io) {
        return function (req, res, next) {
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
            verify(req.user.id, req.params.id).then(function (result) {
                if (result) {
                    switch (req.params.type) {
                        case '0':
                            let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract.methods.owner().call().then(function (owner) {
                                let isOwner = (owner == req.user.id) ? true : false;
                                sellSocket.updateSellData(io,req.originalUrl);
                                res.render(
                                        'manageContracts/contractDetails',
                                        {data: req.user, 'name': result.name, 'id': result.hash, 'isOwner': isOwner});
                            }).catch(function (err) {
                                req.flash('type', 'red');
                                req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                res.redirect('/contracts/participant');
                            });
                            break;
                        case '1':
                            let contract1 = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract1.methods.owner().call().then(function (owner) {
                                let isOwner = (owner == req.user.id) ? true : false;
                                lastWillSocket.updateLastWillData(io,req.originalUrl);
                                res.render('manageContracts/lastWill/participateDetails',
                                        {data: req.user, 'name': result.name, 'id': result.hash, 'isOwner': isOwner});
                            }).catch(function (err) {
                                req.flash('type', 'red');
                                req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                res.redirect('/contracts/participant');
                            });
                            break;
                        case '2':
                            let contract2 = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract2.methods.toDivorce().call().then(function (toDivorce) {
                                contract2.methods.proposer().call().then(function (proposer) {
                                    let isProposer = (proposer == req.user.id) ? true : false;
                                    let divorceApplicant = (toDivorce[0] == req.user.id) ? true : false;
                                    marriageSocket.updateMarriageData(io,req.originalUrl);
                                    res.render('manageContracts/marriage/participateDetails',
                                            {data: req.user, 'name': result.name,
                                                'status': result.status, 'id': result.hash,
                                                'isApplicant': divorceApplicant,
                                                'isProposer': isProposer});
                                }).catch(function (err) {
                                    req.flash('type', 'red');
                                    req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                    res.redirect('/contracts/participant');
                                });
                            }).catch(function (err) {
                                req.flash('type', 'red');
                                req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                res.redirect('/contracts/participant');
                            });
                            break;
                        case '3':
                            let contract3 = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract3.methods.toDivorce().call().then(function (toDivorce) {
                                contract3.methods.proposer().call().then(function (proposer) {
                                    let isProposer = (proposer == req.user.id) ? true : false;
                                    let divorceApplicant = (toDivorce[0] == req.user.id) ? true : false;
                                    marriageSocket.updateMarriageData(io,req.originalUrl);
                                    res.render('manageContracts/marriage/v2/participateDetails',
                                            {data: req.user, 'name': result.name,
                                                'status': result.status, 'id': result.hash,
                                                'isApplicant': divorceApplicant,
                                                'isProposer': isProposer});
                                }).catch(function (err) {
                                    req.flash('type', 'red');
                                    req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                    res.redirect('/contracts/participant');
                                });
                            }).catch(function (err) {
                                req.flash('type', 'red');
                                req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                res.redirect('/contracts/participant');
                            });
                            break;
                        case '4':
                            let contract4 = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract4.methods.owner().call().then(function (owner) {
                                let isOwner = (owner == req.user.id) ? true : false;
                                lastWillSocket.updateLastWillData(io,req.originalUrl);
                                res.render('manageContracts/lastWill/v2/participateDetails',
                                        {data: req.user, 'name': result.name, 'id': result.hash, 'isOwner': isOwner});
                            }).catch(function (err) {
                                req.flash('type', 'red');
                                req.flash('message', 'Ocurrio un error al cargar, intente de nuevo.');
                                res.redirect('/contracts/participant');
                            });
                            break;
                        default:
                            req.flash('type', 'red');
                            req.flash('message', 'El contrato no existe');
                            res.redirect('/contracts/participant');
                            break;
                    }
                } else {
                    req.flash('type', 'red');
                    req.flash('message', 'El contrato no existe');
                    res.redirect('/contracts/participant');
                }
            });

        };
    },
    owner: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.owner().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    conditions: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.conditions().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    product: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.productName().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    description: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.salesDescription().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    price: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.price().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    counters: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.counters().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    percentage: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.percentagePay().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    makePayment: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password_pay).then(function (unlocked) {
                    methods.web3.eth.getBalance(req.user.id).then(function (balance) {
                        let amount = (req.body.amount) * 1000000000000000000;
                        if (amount <= balance) {
                            let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract.methods.pay().send({
                                from: req.user.id,
                                value: amount,
                                to: req.body.paramsId,
                                gas: 3000000,
                                gasprice: '0x0'
                            }).on('error', function (error) {
                                res.status(200).send({'status': false, 'result': 'No se pudo realizar el pago, intenta de nuevo.'});
                            }).then(function (receipt) {
                                res.status(200).send({'status': true, 'result': 'Pago enviado correctamente'});
                            });
                        } else {
                            res.status(200).send({'status': false, 'result': 'Saldo insuficiente'});
                        }
                    }).catch(function (err) {
                        res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo' + err});
                    });
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta' + err + " " + req.body.password_pay});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    verify: verify

};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        where: {
            status: {[op.ne]: '0'},
            hash: idParams
        },
        attributes:
                ['name', 'hash', 'abi', 'status'],
        include: [{
                model: models.Users,
                attributes: {
                    exclude: ['id', 'password', 'createdAt', 'updatedAt', 'email']
                },
                through: {attributes: [],
                    where: {
                        id_user: idUser
                    }
                },
                required: true
            }]
    });
}
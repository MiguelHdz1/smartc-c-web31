    var contractSource ="\
pragma solidity ^0.4.21;\n\
    contract LastWill {\n\
        uint256 public lastTouch;\n\
        address[] public children;\n\
        uint256[] public percentage;\n\
        bool public checkIfDead = false;\n\
        string public conditions;\n\
        address public owner;\n\
        uint256 public amount;\n\
        event Status(string _msg, address user, uint256 time);\n\
        uint256 public timeToVerify;\n\
        uint256[] getterPercent;\n\
        constructor(\n\
                    address[] _children,\n\
                    address _owner,\n\
                    string _conditions,\n\
                    uint256[] _percentage,\n\
                    uint256 _timeToVerify) public payable {\n\
            owner = _owner;\n\
            conditions = _conditions;\n\
            children = _children;\n\
            lastTouch = block.timestamp;\n\
            percentage = _percentage;\n\
            timeToVerify = _timeToVerify;\n\
            emit Status('Last Will Created', msg.sender, block.timestamp);\n\
        }\n\
        \n\
        function depositFunds() public payable {\n\
            amount+= msg.value;\n\
            lastTouch = block.timestamp;\n\
            emit Status('I Am Still Alive!', msg.sender, block.timestamp);\n\
            emit Status('Funds Deposited', msg.sender, block.timestamp);\n\
        }\n\
    \n\
        function stillAlive() public onlyOwner {\n\
            lastTouch = block.timestamp;\n\
            emit Status('I Am Still Alive!', msg.sender, block.timestamp);\n\
        }\n\
        \n\
        function isDead() public {\n\
            emit Status('Asking if dead', msg.sender, block.timestamp);\n\
            if(block.timestamp > (lastTouch + timeToVerify)) {\n\
                checkIfDead = true;\n\
                giveMoneyToChildren();\n\
            } else {\n\
                emit Status('I Am still Alive!', msg.sender, block.timestamp);\n\
            }\n\
        }\n\
\n\
        function childrenAll() public constant returns(address[]){\n\
            return children;\n\
        }\n\
        \n\
        function allPercentages() public constant returns(uint256[]){\n\
            return percentage;\n\
        }\n\
\n\
        function giveMoneyToChildren() public {\n\
            emit Status('I am dead, take my money', msg.sender, block.timestamp);\n\
            for(uint i = 0; i < percentage.length; i++) {\n\
                getterPercent.push(getAmountPerChild(percentage[i]));\n\
            }\n\
            for(uint j = 0; j < children.length; j++){\n\
                children[j].transfer(getterPercent[j]);\n\
            }\n\
            amount =0;\n\
        }\n\
\n\
        function getAmountPerChild(uint256 unitToPercent) public view returns(uint256){\n\
            address smContract = this;\n\
            uint amountTotal = smContract.balance;\n\
            return (unitToPercent * amountTotal) / 100;\n\
        }\n\
        \n\
        function addChild(address _address) public onlyOwner {\n\
            emit Status('Child Added', _address, block.timestamp);\n\
            children.push(_address);\n\
        }\n\
    \n\
        modifier onlyOwner {\n\
            if (msg.sender != owner) {\n\
                revert();\n\
            } else {\n\
                _;\n\
            }\n\
        }\n\
    }"
exports.contractSource = contractSource;
var contractSoruce = 
        'pragma solidity ^0.4.0;\n\
            contract SalesContract {\n\
                address public owner;\n\
                string public productName;\n\
                uint256 public updatedTime;\n\
                string public salesDescription;\n\
                uint public price;\n\
                bool public onSale = true;\n\
                uint public percentagePay;\n\
                address[] public buyers;\n\
                string public conditions;\n\
                bool public conditionStatus;\n\
                string public contractName;\n\
                address public mainBuyer;\n\
                uint public totalPayed = 0;\n\
                mapping (address=>bool) public isBuyer;\n\
                event UserStatus(string _msg, address user, uint amount,uint256 time);\n\
                \n\
                function SalesContract(\n\
                    string _productName,\n\
                    string _contractName,\n\
                    address _owner,\n\
                    string description,\n\
                    uint _price,\n\
                    address[] _buyers,\n\
                    string _conditions,\n\
                    bool _conditionStatus,\n\
                    uint _percentage) payable public{\n\
                        owner = _owner;\n\
                        productName=_productName;\n\
                        salesDescription = description;\n\
                        price = _price;\n\
                        updatedTime = block.timestamp;\n\
                        buyers = _buyers;\n\
                        conditions =_conditions;\n\
                        conditionStatus=_conditionStatus;\n\
                        contractName = _contractName;\n\
                        percentagePay = _percentage;\n\
\n\
                        for (uint i=0; i<buyers.length; i++) {\n\
                            require(!isBuyer[_buyers[i]] && _buyers[i] != 0);\n\
                            isBuyer[_buyers[i]] = true;\n\
                        }\n\
                        mainBuyer= _buyers[0];\n\
                        emit UserStatus(description, msg.sender, msg.value, block.timestamp);\n\
                        emit UserStatus("Item on sale:", msg.sender, msg.value, block.timestamp);\n\
                }\n\
\n\
                function percentage(uint value, uint _price) internal pure returns(uint){\n\
                    uint _percentage = (((value*1000000000000000000) * 100) / _price);\n\
                    return (_percentage);\n\
                }\n\
\n\
                function pay() payable public{\n\
                    if(onSale == true && isBuyer[msg.sender]){\n\
                        owner.transfer(address(this).balance);\n\
                        percentagePay += (percentage(msg.value, price));\n\
                        totalPayed+=msg.value;\n\
                        if(conditionStatus ==true){\n\
                            if(totalPayed >= price){\n\
                                owner = msg.sender;\n\
                                onSale= false;\n\
                                percentagePay=100000000000000000000;\n\
                                for (uint i=0; i<buyers.length; i++) {\n\
                                    isBuyer[buyers[i]] = false;\n\
                                    delete buyers[i];\n\
                                }\n\
                            }\n\
                            emit UserStatus("Item bought",msg.sender,msg.value,block.timestamp);\n\
                            emit UserStatus("Item no longer on sale",msg.sender,msg.value,block.timestamp);\n\
                        }\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
\n\
                function updatePrice(uint _price) public {\n\
                    if(owner == msg.sender){\n\
                        price = _price;\n\
                        emit UserStatus("Price updated", msg.sender, price, block.timestamp);\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
\n\
                function modifyDescription(string description) public{\n\
                    if(owner == msg.sender){\n\
                        salesDescription = description;\n\
                        emit UserStatus(description, msg.sender, 0, block.timestamp);\n\
                        emit UserStatus("description modified", msg.sender, 0, block.timestamp);\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
        \n\
                function putOnSale() public{\n\
                    if(owner == msg.sender){\n\
                        onSale=true;\n\
                        emit UserStatus("Item now is on sale", msg.sender, 0, block.timestamp);\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
\n\
                function removeFromSale() public{\n\
                    if(owner == msg.sender){\n\
                        onSale=true;\n\
                        emit UserStatus("Item no longer on sale", msg.sender, 0, block.timestamp);\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
\n\
                function counters() public constant returns(address[]){\n\
                    return buyers;\n\
                }\n\
\n\
                function addBuyer(address newCounter) public{\n\
                    if(owner == msg.sender){\n\
                        isBuyer[newCounter]=true;\n\
                        buyers.push(newCounter);\n\
                    }else{\n\
                        revert();\n\
                    }\n\
                }\n\
        }';
exports.contractSoruce = contractSoruce;
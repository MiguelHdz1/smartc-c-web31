'use strict';

var models = require('../../../bd/models');
var methods = require('../../conf/conf');
var op = require('sequelize').Op;
module.exports = {

    contractInfo: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        verify(req.user.id, req.params.id).then(function (result) {
            if (result) {
                var route = '';
                switch (req.params.type) {
                    case '0':
                        res.render('manageContracts/owner/contractDetails', {data: req.user, 'name': result.name, 'id': result.hash});
                        break;
                    case '1':
                        res.render('manageContracts/lastWill/details', {data: req.user, 'name': result.name, 'id': result.hash});
                        break;
                    case '2':
                        res.render('manageContracts/marriage/ownerDetails', {data: req.user, 'name': result.name, 'status': result.status, 'id': result.hash});
                        break;
                    case '3':
                        res.render('manageContracts/marriage/v2/ownerDetails', {data: req.user, 'name': result.name, 'status': result.status, 'id': result.hash});
                        break;
                    case '4':
                        res.render('manageContracts/lastWill/v2/details', {data: req.user, 'name': result.name, 'status': result.status, 'id': result.hash});
                        break;
                    default:
                        req.flash('type', 'red');
                        req.flash('message', 'El contrato no existe');
                        res.redirect('/manage-contracts/');
                        break;
                }
            } else {
                req.flash('type', 'red');
                req.flash('message', 'El contrato no existe');
                res.redirect('/manage-contracts/');
            }
        });
    },

    owner: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.owner().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    conditions: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.conditions().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    product: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.productName().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    description: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.salesDescription().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    price: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.price().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    counters: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.counters().call().then(function(result){
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function(err){
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    percentage: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.percentagePay().call().then(function(result){
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function(err){
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error de conexión, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    verify: verify
};

function verify(idUser, idParams) {
    return models.contracts.findOne({
        attributes: ['id', 'hash', 'abi', 'name', 'status'],
        where: {
            user_id: idUser,
            hash: idParams,
            status: {[op.ne]: '0'}
        }
    });
}
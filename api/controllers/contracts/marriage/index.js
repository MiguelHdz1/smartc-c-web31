'use strict';

module.exports={
    create:function(req,res){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/marriage/create', {data: req.user});
    },
    
    v2create:function(req,res){
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/marriage/v2/create', {data: req.user});
    }
};
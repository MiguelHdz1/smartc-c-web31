'use strict';
var $ = require('min-jquery');
var methods = require('../../../conf/conf');
var $ = require('min-jquery');
var op = require('sequelize').Op;
var models = require('../../../../bd/models');
var account = require('../../methods/accountsController');

module.exports = {
    agreements: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.agreements().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposalDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposalDate().call().then(function (proposalDate) {
                    res.status(200).send({'status': true, 'result': getDate(proposalDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    answeredDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.answeredDate().call().then(function (answeredDate) {
                    res.status(200).send({'status': true, 'result': getDate(answeredDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    endedDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.endedDate().call().then(function (endedDate) {
                    res.status(200).send({'status': true, 'result': getDate(endedDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposer: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposer().call().then(function (proposer) {
                    res.status(200).send({'status': true, 'result': proposer});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposed: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposed().call().then(function (proposed) {
                    res.status(200).send({'status': true, 'result': proposed});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    accepted: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.accepted().call().then(function (accepted) {
                    res.status(200).send({'status': true, 'result': accepted});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    ended: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.ended().call().then(function (ended) {
                    res.status(200).send({'status': true, 'result': ended});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    balance: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                methods.web3.eth.getBalance(result.hash).then(function (balance) {
                    res.status(200).send({'status': true, 'result': balance / 1000000000000000000});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    regimen: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.propertyShared().call().then(function (propertyShared) {
                    let regimen = (propertyShared) ? 'Bienes mancomunados' : 'Bienes separados';
                    res.status(200).send({'status': true, 'result': regimen});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    answerMarriage: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    let answer = (req.body.answer == 1) ? true : false;
                    let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                    contract.methods.answerMarriage(answer)
                            .send({
                                from: req.user.id,
                                value: 0,
                                gas: 2000000,
                                gasprice: '0x0'})
                            .on('error', function (error) {
                                res.status(200).send({'status': false, 'result': 'No se ha podido enviar la respuesta, intente de nuevo.'});
                            })
                            .once('transactionHash', function (transactionHash) {})
                            .once('receipt', function (receipt) {})
                            .on('confirmation', function (confirmation) {})
                            .then(function (receipt) {
                                let status = (answer) ? '2' : '3';
                                updateStatus(result.hash, status).then(function () {
                                    if (!answer) {
                                        res.status(200).send({'status': true, 'result': 'Haz rechazado esta propuesta'});

                                    } else {
                                        res.status(200).send({'status': true, 'result': 'Haz aceptado esta propuesta'});

                                    }
                                }).catch(function (error) {
                                    res.status(200).send({'status': false, 'result': 'No se ha podido enviar la respuesta, intente de nuevo.'});
                                });
                            });
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    askForDivorce: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                    contract.methods.askForDivorce().send({
                        from: req.user.id,
                        value: 0,
                        to: req.body.paramsId}).once('transactionHash', function (transactionHash) {}).once('receipt', function (receipt) {})
                            .on('confirmation', function (confNumber, receipt) {}).on('error', function (error) {
                        res.status(200).send({'status': false, 'result': 'No se ha podido enviar la confirmación, intente de nuevo.'});
                    }).then(function (receipt) {
                        updateStatus(result.hash, '4').then(function () {}).then(function () {
                            res.status(200).send({'status': true, 'result': 'Se ha enviado la solicitud de divorcio.'});
                        }).catch(function (error) {
                            res.status(200).send({'status': false, 'result': 'No se ha podido realizar la solicitud de divorcio, intente de nuevo.'});
                        });
                    });
                }).catch(function (ex) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    answerForDivorce: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    var answer = (req.body.answer == 1) ? true : false;
                    var status = (req.body.answer == 1) ? '5' : '2';
                    let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                    contract.methods.answerDivorce(answer).send({
                        from: req.user.id,
                        value: 0,
                        gas: 3100000,
                        gasprice: '0x0',
                        to: req.body.paramsId
                    }).once('transactionHash', function (transactionHash) {})
                            .once('receipt', function (receipt) {})
                            .on('confirmation', function (confNumber, receipt) {})
                            .on('error', function (error) {
                                res.status(200).send({'status': false, 'result': 'No se ha podido enviar tu respuesta a la solicitud de divorcio, intente de nuevo.'});
                            }).then(function (receipt) {
                        updateStatus(result.hash, status).then(function () {}).then(function () {
                            res.status(200).send({'status': true, 'result': 'Se ha enviado tu respuesta a la solicitud de divorcio.'});
                        }).catch(function (error) {
                            res.status(200).send({'status': false, 'result': 'No se ha podido enviar tu respuesta a la solicitud de divorcio, intente de nuevo.'});
                        });
                    });
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    cancelDivorce: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    contract.methods.cancelDivorce().send({
                        from: req.user.id,
                        value: 0,
                        gas: 3100000,
                        gasprice: '0x0',
                        to: req.body.paramsId
                    }).on('error', function (error) {
                        res.status(200).send({'status': false, 'result': 'No se ha podido cancelar la solicitud de divorcio, intente de nuevo.'});
                    }).then(function (receipt) {
                        updateStatus(result.hash, '2').then(function () {}).then(function () {
                            res.status(200).send({'status': true, 'result': 'Se ha cancelado la solicitud de divorcio.'});
                        }).catch(function (error) {
                            res.status(200).send({'status': false, 'result': 'No se ha podido cancelar la solicitud de divorcio, intente de nuevo.'});
                        });
                    });
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    deposit: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    methods.web3.eth.getBalance(req.user.id).then(function (balance) {
                        let amount = (req.body.amount) * 1000000000000000000;
                        if (amount <= balance) {
                            let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract.methods.deposit().send({
                                from: req.user.id,
                                value: amount,
                                to: req.body.paramsId
                            }).on('error', function (error) {
                                res.status(200).send({'status': false, 'result': 'No se ha podido realizar el deposito, intente de nuevo.'});
                            }).then(function (receipt) {
                                res.status(200).send({'status': true, 'result': 'Se ha realizado el deposito correctamente, espera para ver los cambios.'});
                            });
                        } else {
                            res.status(200).send({'status': false, 'result': 'Saldo insuficiente'});
                        }
                    }).catch(function (err) {
                        res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                    });
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    verify: verify,
    verifyIfLess: verifyIfLess,
    updateStatus: updateStatus
};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        where: {
            status: {[op.ne]: '0'},
            hash: idParams
        },
        attributes: {
            //include: ['name', 'hash'],
            exclude: ['createdAt', 'updatedAt', 'user_id', 'status', 'byteCode']
        },
        include: [{
                model: models.Users,
                attributes: {
                    exclude: ['id', 'password', 'createdAt', 'updatedAt', 'email']
                },
                through: {attributes: [],
                    where: {
                        id_user: idUser
                    }
                },
                required: true
            }]
    });
}

function verifyIfLess(character) {
    if (character < 10) {
        return '0' + character;
    } else {
        return character;
    }
}

function getDate(newDate) {
    let date = new Date((newDate * 1000));
    let _date = verifyIfLess(date.getDate()) + '/' +
            verifyIfLess((date.getMonth() + 1)) + '/' +
            verifyIfLess(date.getFullYear()) + '  ' +
            verifyIfLess(date.getHours()) + ':' +
            verifyIfLess(date.getMinutes()) + ':' +
            verifyIfLess(date.getSeconds());
    return _date;
}

function updateStatus(hash, value) {
    return models.contracts.update(
            {
                status: value
            },
            {
                where: {
                    hash: hash
                }
            }
    );
}
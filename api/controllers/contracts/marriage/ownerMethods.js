'use strict';
var $ = require('min-jquery');
var methods = require('../../../conf/conf');
var $ = require('min-jquery');
var op = require('sequelize').Op;
var models = require('../../../../bd/models');
var account = require('../../methods/accountsController');

module.exports = {

    agreements: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.agreements().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposalDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposalDate().call().then(function (proposalDate) {                    
                    res.status(200).send({'status': true,'result': getDate(proposalDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    answeredDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.answeredDate().call().then(function (answeredDate) {                    
                    res.status(200).send({'status': true,'result': getDate(answeredDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    endedDate: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.endedDate().call().then(function (endedDate) {
                    res.status(200).send({'status': true,'result': getDate(endedDate)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });               
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposer: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposer().call().then(function (proposer) {
                    res.status(200).send({'status': true, 'result': proposer});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    proposed: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.proposed().call().then(function (proposed) {
                    res.status(200).send({'status': true, 'result': proposed});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    accepted: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.accepted().call().then(function (accepted) {
                    res.status(200).send({'status': true, 'result': accepted});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    ended: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.ended().call().then(function (ended) {
                    res.status(200).send({'status': true, 'result': ended});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    balance: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                methods.web3.eth.getBalance(result.hash).then(function(balance){
                    res.status(200).send({'status': true, 'result': balance / 1000000000000000000});
                }).catch(function(err){
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    regimen: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.propertyShared().call().then(function (propertyShared) {
                    let regimen = (propertyShared) ? 'Bienes mancomunados' : 'Bienes separados';
                    res.status(200).send({'status': true, 'result': regimen});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });                
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },

    verify: verify,
    verifyIfLess: verifyIfLess,
    getDate:getDate

};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        attributes: ['id', 'hash', 'abi', 'name', 'status'],
        where: {
            user_id: idUser,
            hash: idParams,
            status: {[op.ne]: '0'}
        }
    });
}

function getDate(newDate) {
    let date = new Date((newDate * 1000));
    let _date = verifyIfLess(date.getDate()) + '/' +
            verifyIfLess((date.getMonth() + 1)) + '/' +
            verifyIfLess(date.getFullYear()) + '  ' +
            verifyIfLess(date.getHours()) + ':' +
            verifyIfLess(date.getMinutes()) + ':' +
            verifyIfLess(date.getSeconds());
    return _date;
}

function verifyIfLess(character) {
    if (character < 10) {
        return '0' + character;
    } else {
        return character;
    }
}

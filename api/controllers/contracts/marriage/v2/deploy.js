'use strict';

var $ = require('min-jquery');
var methods = require('../../../../conf/conf');
var accounts = require('../../../methods/accountsController');
var solc = require('solc');
var sources = require('../../contractSources/marriageV2Contract');
var models = require('../../../../../bd/models');
var op = require('sequelize').Op;

module.exports = {

    deployContract: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        var contractSource = sources.contractSource;
        var compiled = solc.compile(contractSource, 1).contracts[':MarriageContract'];
        var byteCode = '0x' + compiled.bytecode;
        var abiCompiled = JSON.parse(compiled.interface);
        var contract = new methods.web3.eth.Contract(abiCompiled);
        var contractName = req.body.contractName;
        var proposer = req.body.proposer;
        var proposed = req.body.proposed;
        var conditions = req.body.conditions;
        var userId = req.body.address;
        var password = req.body.password_main;
        var multiple = [];

        methods.web3.eth.personal.unlockAccount(userId, password, function (error, result) {
            if (!error) {
                models.Users.findOne({where: {id: proposer}}).then(function (ress) {
                    if (ress) {
                        models.Users.findOne({where: {id: proposed}}).then(function (result) {
                            if (result) {
                                let regimen = (req.body.marriage_regimen == "0") ? true : false;
                                contract.deploy({
                                    data: byteCode,
                                    arguments: [conditions, proposer, proposed, regimen]
                                }).send({
                                    from: userId,
                                    gas: 2000000,
                                    gasprice: '0x0'
                                })
                                .on('error', function (error) {
                                    res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                })
                                .on('transactionHash', function (transactionHash) {})
                                .on('receipt', function (receipt) {})
                                .on('confirmation', function (confirmationNumber, receipt) {})
                                .then(function (newContractInstance) {
                                    models.contracts.create({
                                        name: contractName,
                                        abi: JSON.stringify(abiCompiled),
                                        byteCode: byteCode,
                                        hash: newContractInstance.options.address,
                                        route: '3',
                                        user_id: req.user.id,
                                        status: '1'
                                    }).then(ressult => {
                                        multiple.push({id_contract: ressult.id, id_user: proposer});
                                        multiple.push({id_contract: ressult.id, id_user: proposed});
                                        models.participants.bulkCreate(multiple);
                                        res.status(202).send({'status': true, 'message': 'Contrato creado con éxito'});
                                    }).catch(function (error) {
                                        console.log(error);
                                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                    });
                                });
                            } else {
                                res.status(202).send({'status': false, 'message': 'El propuesto es un usuario invalido'});
                            }
                        });
                    } else {
                        res.status(202).send({'status': false, 'message': 'El proponente es un usuario invalido'});
                    }
                });
            } else {
                console.log(error);
                res.status(202).send({'status': false, 'message': 'Contraseña incorrecta'});
            }
        });
    }

};
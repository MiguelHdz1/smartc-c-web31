'use strict';

var $ = require('min-jquery');
var methods = require('../../conf/conf');
var solc = require('solc');
var sources = require('./contractSources/salesContract');
var models = require('../../../bd/models');
var op = require('sequelize').Op;
var account = require('../methods/accountsController');


module.exports = {

    manageContracts: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        models.contracts.findAll({
            attributes: ['id', 'hash', 'name', 'route'],
            where: {
                user_id: req.user.id,
                status: {[op.ne]: '0'}
            },
            order: [['createdAt', 'DESC']]
        }).then(function (result) {
            res.render('manageContracts/index', {data: req.user, 'contracts': result, message: req.flash('message'), type: req.flash('type')});
        });

        //res.render('manageContracts/index', {data: req.user, 'contracts': contracts});
    },

    showParticipants: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        models.contracts.findAll({
            where: {
                status: {[op.ne]: '0'}
            },
            attributes: {
                //include: ['name', 'hash'],
                exclude: ['abi', 'createdAt', 'updatedAt', 'user_id', 'status', 'byteCode']
            },
            include: [{
                    model: models.Users,
                    //where: {id: req.user.id},
                    attributes: {
//                        include: ['id'],
                        exclude: ['id', 'password', 'createdAt', 'updatedAt', 'email']
                    },
                    through: {attributes: [],
                        where: {id_user: req.user.id}
                    },
                    required: true
                }],
            order: [['createdAt', 'DESC']]
        }).then(contracts => {
            res.render('manageContracts/participant', {data: req.user, 'contracts': contracts, message: req.flash('message'), type: req.flash('type')});

        });
    },

    createContract: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/create', {data: req.user});
    },

    showTemplates: function (req,res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/templates', {data: req.user, message: req.flash('message'), type: req.flash('type')});
    },

    deployContract: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        var contractSoruce = sources.contractSoruce;
        var compiled = solc.compile(contractSoruce, 1).contracts[':SalesContract'];
        var byteCode = '0x' + compiled.bytecode;
        var abiCompiled = JSON.parse(compiled.interface);
        var nameProduct = req.body.nameProduct;
        var contractName = req.body.contractName;
        var ownerContract = req.body.ownerContract;
        var details = req.body.details;
        var priceProduct = (req.body.priceProduct) * 1000000000000000000;
        var counter = req.body.counter;
        var conditions = req.body.conditions;
        var conditionStatus = true;
        var multiple = [];

        account.unlockAccount(req.body.address, req.body.password_main).then(function (unlocked) {
            models.Users.findOne({where: {id: ownerContract}}).then(function (ress) {
                if (ress) {
                    models.Users.findAll({where: {id: counter}}).then(function (result) {
                        if (result) {
                            if ((counter.length) = (result.length)) {
                                let contract = new methods.web3.eth.Contract(abiCompiled);
                                contract.deploy({
                                    data: byteCode,
                                    arguments: [nameProduct, contractName, ownerContract,
                                        details, priceProduct, counter, conditions, conditionStatus, 0]
                                }).send({
                                    from: req.body.address,
                                    data: byteCode,
                                    gas: 3100000,
                                    gasprice: '0x0'}).on('error', function (error) {
                                    res.status(202).send({'status': false,
                                        'message': 'Ocurrio un error , intente de nuevo'});
                                }).then(function (newContractInstance) {
                                    models.contracts.create({
                                        name: contractName,
                                        abi: JSON.stringify(abiCompiled),
                                        byteCode: byteCode,
                                        hash: newContractInstance.options.address,
                                        route: '0',
                                        user_id: req.user.id,
                                        status: '1'
                                    }).then(ressult => {
                                        $.each(counter, function (key, value) {
                                            multiple.push({id_contract: ressult.id, id_user: value});
                                        });
                                        multiple.push({id_contract: ressult.id, id_user: ownerContract});
                                        models.participants.bulkCreate(multiple);
                                        res.status(202).send({'status': true, 'message': 'Contrato creado con éxito'});
                                    }).catch(function (error) {
                                        console.log(error);
                                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                    });
                                });
                            } else {
                                res.status(202).send({'status': false, 'message': 'Alguna de las contrapartes es un usuario invalido'});
                            }
                        } else {
                            res.status(202).send({'status': false, 'message': 'Los usuarios no estan registrados'});

                        }
                    }).catch(function (err) {
                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                        console.log(err);
                    });
                } else {
                    res.status(202).send({'status': false, 'message': 'El dueño es un usuario invalido'});
                }
            });
        }).catch(function (err) {
            res.status(200).send({status: false, 'message': 'Contraseña incorrecta'});
        });
    }

};

'use strict';
var $ = require('min-jquery');
var methods = require('../../../../conf/conf');
var solc = require('solc');
var sources = require('../../contractSources/lastWillContractv2');
var account = require('../../../methods/accountsController');
var models = require('../../../../../bd/models');
var op = require('sequelize').Op;

module.exports = {

    deploy: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        var contractSource = sources.contractSource;
        var compiled = solc.compile(contractSource, 1).contracts[':LastWill'];
        var byteCode = '0x' + compiled.bytecode;
        var abiCompiled = JSON.parse(compiled.interface);
        var contractName = req.body.contractName;
        var ownerContract = req.body.ownerContract;
        var counter = req.body.counter;
        var conditions = req.body.conditions;
        var multiple = [];
        let seconds = req.body.seconds;
        let percentage = req.body.percentage;

        account.unlockAccount(req.body.address, req.body.password_main).then(function (unlocked) {
            models.Users.findOne({where: {id: ownerContract}}).then(function (ress) {
                if (ress) {
                    models.Users.findAll({where: {id: counter}}).then(function (result) {
                        if (result) {
                            if ((counter.length) = (result.length)) {
                                let contract = new methods.web3.eth.Contract(abiCompiled);
                                contract.deploy({
                                    data: byteCode,
                                    arguments: [counter, ownerContract, conditions, percentage,seconds]
                                }).send({
                                    from: req.body.address,
                                    gas: 3100000,
                                    gasprice: '0x0'
                                }).on('error', function (error) {
                                    res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'+ error});
                                }).then(function (newContractInstance) {
                                    models.contracts.create({
                                        name: contractName,
                                        abi: JSON.stringify(abiCompiled),
                                        byteCode: byteCode,
                                        hash: newContractInstance.options.address,
                                        route: '4',
                                        user_id: req.user.id,
                                        status: '1'
                                    }).then(ressult => {
                                        $.each(counter, function (key, value) {
                                            multiple.push({id_contract: ressult.id, id_user: value});
                                        });
                                        multiple.push({id_contract: ressult.id, id_user: ownerContract});
                                        models.participants.bulkCreate(multiple);
                                        res.status(202).send({'status': true, 'message': 'Contrato creado con éxito'});
                                    }).catch(function (error) {
                                        console.log(error);
                                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                    });
                                });
                            } else {
                                res.status(202).send({'status': false, 'message': 'Alguna de las contrapartes es un usuario invalido'});
                            }
                        } else {
                            res.status(202).send({'status': false, 'message': 'Los usuarios no estan registrados'});

                        }
                    }).catch(function (err) {
                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                        console.log(err);
                    });
                } else {
                    res.status(202).send({'status': false, 'message': 'El dueño es un usuario invalido'});
                }
            });
        }).catch(function (err) {
            res.status(202).send({'status': false, 'message': 'Contraseña incorrecta'});
        });
    }   
};
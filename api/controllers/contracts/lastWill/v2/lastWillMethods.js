'use strict';
var $ = require('min-jquery');
var methods = require('../../../../conf/conf');
var solc = require('solc');
var sources = require('../../contractSources/lastWillContract');
var account = require('../../../methods/accountsController');
var models = require('../../../../../bd/models');
var op = require('sequelize').Op;

module.exports = {
    create: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/lastWill/v2/create', {data: req.user});
    },
   
    owner: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.owner().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    conditions: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.conditions().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    amount: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.amount().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    lifeStatus: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.checkIfDead().call().then(function (result) {
                    let lifeState = (result) ? 'Fallecido' : 'Vivo';
                    res.status(200).send({'status': true, 'result': lifeState});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    children: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.childrenAll().call().then(function (children) {
                    contract.methods.allPercentages().call().then(function(percentages){
                        res.status(200).send({'status': true, 'result': children,'percentages': percentages});
                    }).catch(function(error){
                        res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo' +error});
                    });                    
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'+err});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
      lastTouch: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.lastTouch().call().then(function (result) {
                    var date = new Date((result * 1000));
                    res.status(200).send({'status': true,
                        'result': verifyIfLess(date.getDate()) + '/' +
                                verifyIfLess((date.getMonth() + 1)) + '/' +
                                verifyIfLess(date.getFullYear()) + '  ' +
                                verifyIfLess(date.getHours()) + ':' +
                                verifyIfLess(date.getMinutes()) + ':' +
                                verifyIfLess(date.getSeconds())});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    verify: verify,
    verifyIfLess:verifyIfLess
    
};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        attributes: ['id', 'hash', 'abi', 'name'],
        where: {
            user_id: idUser,
            hash: idParams,
            status: {[op.ne]: '0'}
        }
    });
}

function verifyIfLess(character) {
    if (character < 10) {
        return '0' + character;
    } else {
        return character;
    }
}
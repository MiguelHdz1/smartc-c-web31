'use strict';
var $ = require('min-jquery');
var methods = require('../../../conf/conf');
var $ = require('min-jquery');
var op = require('sequelize').Op;
var sources = require('../contractSources/lastWillContract');
var models = require('../../../../bd/models');
var account = require('../../methods/accountsController');

module.exports = {

    owner: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.owner().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    conditions: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.conditions().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    amount: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.amount().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    lifeStatus: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.checkIfDead().call().then(function (result) {
                    let lifeState = (result) ? 'Fallecido' : 'Vivo';
                    res.status(200).send({'status': true, 'result': lifeState});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    children: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.children().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    lastTouch: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.lastTouch().call().then(function (result) {
                    var date = new Date((result * 1000));
                    res.status(200).send({'status': true,
                        'result': verifyIfLess(date.getDate()) + '/' +
                                verifyIfLess((date.getMonth() + 1)) + '/' +
                                verifyIfLess(date.getFullYear()) + '  ' +
                                verifyIfLess(date.getHours()) + ':' +
                                verifyIfLess(date.getMinutes()) + ':' +
                                verifyIfLess(date.getSeconds())});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    askIfDead: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                    contract.methods.isDead().send({
                        from: req.user.id,
                        gas: 3100000,
                        gasprice: '0x0'}).on('error', function (error) {
                        res.status(200).send({'status': false, 'result': 'No se ha podido sincronizar, intente de nuevo.'});
                    }).then(function (receipt) {
                        res.status(200).send({'status': true, 'result': 'El estado del dueño se ha sincornizado'});
                    });
                }).catch(function (error) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    confirmLife: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                    let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                    contract.methods.stillAlive().send({from: req.user.id}).on('error', function (err) {
                        res.status(200).send({'status': false, 'result': 'No se ha podido enviar la confirmación, intente de nuevo.'});
                    }).then(function (receipt) {
                        res.status(200).send({'status': true, 'result': 'Confirmación enviada con exito'});
                    });
                }).catch(function (error) {
                    res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    fund: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                methods.web3.eth.getBalance(req.user.id).then(function (balance) {                    
                    if ((req.body.amount * 1000000000000000000) <= balance) {
                        account.unlockAccount(req.user.id, req.body.password).then(function (unlocked) {
                            let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                            contract.methods.depositFunds().send({
                                from: req.user.id,
                                value: (req.body.amount * 1000000000000000000),
                                to: req.body.paramsId
                            }).on('error', function (err) {
                                res.status(200).send({'status': false, 'result': 'No se han podido enviar los fondos, intente de nuevo.'});
                            }).then(function (receipt) {
                                res.status(200).send({'status': true, 'result': 'Fondos enviados con exito'});
                            });
                        }).catch(function (err) {
                            res.status(200).send({'status': false, 'result': 'Contraseña incorrecta'});
                        });
                    } else {
                        res.status(200).send({'status': false, 'result': 'Saldo insuficiente.'});
                    }
                }).catch(function (error) {
                    res.status(200).send({'status': false, 'result': 'No se han podido enviar los fondos, intente de nuevo.'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    verify: verify,
    verifyIfLess: verifyIfLess
};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        where: {
            status: {[op.ne]: '0'},
            hash: idParams
        },
        attributes: {
            //include: ['name', 'hash'],
            exclude: ['createdAt', 'updatedAt', 'user_id', 'status', 'byteCode']
        },
        include: [{
                model: models.Users,
                attributes: {
                    exclude: ['id', 'password', 'createdAt', 'updatedAt', 'email']
                },
                through: {attributes: [],
                    where: {
                        id_user: idUser
                    }
                },
                required: true
            }]
    });
}

function verifyIfLess(character) {
    if (character < 10) {
        return '0' + character;
    } else {
        return character;
    }
}
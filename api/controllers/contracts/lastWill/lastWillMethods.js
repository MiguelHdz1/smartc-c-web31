'use strict';
var $ = require('min-jquery');
var methods = require('../../../conf/conf');
var solc = require('solc');
var sources = require('../contractSources/lastWillContract');
var account = require('../../methods/accountsController');
var models = require('../../../../bd/models');
var op = require('sequelize').Op;

module.exports = {
    create: function (req, res) {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.header('Expires', 'Fri, 31 Dec 1998 12:00:00 GMT');
        res.render('manageContracts/lastWill/create', {data: req.user});
    },

    deploy: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        var contractSource = sources.contractSource;
        var compiled = solc.compile(contractSource, 1).contracts[':LastWill'];
        var byteCode = '0x' + compiled.bytecode;
        var abiCompiled = JSON.parse(compiled.interface);
        var contractName = req.body.contractName;
        var ownerContract = req.body.ownerContract;
        var counter = req.body.counter;
        var conditions = req.body.conditions;
        var multiple = [];

        account.unlockAccount(req.body.address, req.body.password_main).then(function (unlocked) {
            models.Users.findOne({where: {id: ownerContract}}).then(function (ress) {
                if (ress) {
                    models.Users.findAll({where: {id: counter}}).then(function (result) {
                        if (result) {
                            if ((counter.length) = (result.length)) {
                                let contract = new methods.web3.eth.Contract(abiCompiled);
                                contract.deploy({
                                    data: byteCode,
                                    arguments: [counter, ownerContract, conditions]
                                }).send({
                                    from: req.body.address,
                                    gas: 3100000,
                                    gasprice: '0x0'
                                }).on('error', function (error) {
                                    res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                }).then(function (newContractInstance) {
                                    models.contracts.create({
                                        name: contractName,
                                        abi: JSON.stringify(abiCompiled),
                                        byteCode: byteCode,
                                        hash: newContractInstance.options.address,
                                        route: '1',
                                        user_id: req.user.id,
                                        status: '1'
                                    }).then(ressult => {
                                        $.each(counter, function (key, value) {
                                            multiple.push({id_contract: ressult.id, id_user: value});
                                        });
                                        multiple.push({id_contract: ressult.id, id_user: ownerContract});
                                        models.participants.bulkCreate(multiple);
                                        res.status(202).send({'status': true, 'message': 'Contrato creado con éxito'});
                                    }).catch(function (error) {
                                        console.log(error);
                                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                                    });
                                });
                            } else {
                                res.status(202).send({'status': false, 'message': 'Alguna de las contrapartes es un usuario invalido'});
                            }
                        } else {
                            res.status(202).send({'status': false, 'message': 'Los usuarios no estan registrados'});

                        }
                    }).catch(function (err) {
                        res.status(202).send({'status': false, 'message': 'Ocurrio un error , intente de nuevo'});
                        console.log(err);
                    });
                } else {
                    res.status(202).send({'status': false, 'message': 'El dueño es un usuario invalido'});
                }
            });
        }).catch(function (err) {
            res.status(202).send({'status': false, 'message': 'Contraseña incorrecta'});
        });
    },
    owner: function (req, res) {
        res.header("Content-Type", "application/json");
        res.header("Access-Control-Allow-Origin", '*');
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.owner().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    conditions: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.conditions().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    amount: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.amount().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': (result / 1000000000000000000).toFixed(2)});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    lifeStatus: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.checkIfDead().call().then(function (result) {
                    let lifeState = (result) ? 'Fallecido' : 'Vivo';
                    res.status(200).send({'status': true, 'result': lifeState});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    children: function (req, res) {
        verify(req.user.id, req.body.paramsId).then(function (result) {
            if (result) {
                let contract = new methods.web3.eth.Contract(JSON.parse(result.abi), result.hash);
                contract.methods.children().call().then(function (result) {
                    res.status(200).send({'status': true, 'result': result});
                }).catch(function (err) {
                    res.status(200).send({'status': false, 'result': 'Ocurrio un error intente de nuevo'});
                });
            } else {
                res.status(200).send({'status': false, 'result': 'El contrato no existe'});
            }
        });
    },
    verify: verify
};
function verify(idUser, idParams) {
    return models.contracts.findOne({
        attributes: ['id', 'hash', 'abi', 'name'],
        where: {
            user_id: idUser,
            hash: idParams,
            status: {[op.ne]: '0'}
        }
    });
}
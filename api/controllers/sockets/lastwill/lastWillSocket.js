'use strict';

module.exports = {
    updateLastWillData: function (io, path) {
        io.on('connection', function (socket) {
            io.removeAllListeners();
            console.log('connected');
            socket.on(path, function () {
                socket.join(path);
            });

            socket.on('lastWill.funds', function (data) {
                io.in(path).emit('lastWill.funds.response',data);
            });

            socket.on('lastWill.alive', function (data) {
                io.in(path).emit('lastWill.alive.response',data);
            });
            
            socket.on('lastWill.sync', function (data) {
                io.in(path).emit('lastWill.sync.response',data);
            });

            socket.on('disconnect', function () {
                socket.leave(path);
            });
        });
    }
};
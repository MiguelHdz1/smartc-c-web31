'use strict';

module.exports = {
    updateSellData: function (io, path) {
        io.on('connection', function (socket) {
            io.removeAllListeners();
            console.log('connected');
            socket.on(path, function () {
                socket.join(path);
            });

            socket.on('sell.pay', function (data) {
                io.in(path).emit('sell.pay.response', {'user': data.user, 'amount': data.amount});
            });
            
            socket.on('sell.pay_complete', function () {
                io.in(path).emit('sell.pay_complete.response');
            });

            socket.on('disconnect', function () {
                socket.leave(path);
            });
        });
    }
};
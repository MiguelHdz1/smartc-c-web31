'use strict';

module.exports = {
    updateMarriageData: function (io, path) {
        io.on('connection', function (socket) {
            io.removeAllListeners();
            console.log('connected');
            socket.on(path, function () {
                socket.join(path);
            });

            socket.on('marriage.funds', function (data) {
                io.in(path).emit('marriage.funds.response', {'user': data.user, 'amount': data.amount});
            });

            socket.on('marriage.answer', function (data) {
                io.in(path).emit('marriage.answer.response', data);
            });


            socket.on('marriage.divorce_request', function (data) {
                io.in(path).emit('marriage.divorce_request.response', data);
            });

            socket.on('marriage.divorce_answer', function (data) {
                io.in(path).emit('marriage.divorce_answer.response', data);
            });

            socket.on('marriage.divorce_cancel', function (data) {
                io.in(path).emit('marriage.divorce_cancel.response', data);
            });

            socket.on('disconnect', function () {
                socket.leave(path);
            });
        });
    }
};
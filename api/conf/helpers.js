module.exports ={
  ifequal: function(a,b,options){
      if(a==b){
          return options.fn(this);
      }else{
          return options.inverse(this);
      }
  },
  ifDiff: function(a,b,options){
      if(a != b){
          return options.fn(this);
      }else{
          return options.inverse(this);
      }
  }
};
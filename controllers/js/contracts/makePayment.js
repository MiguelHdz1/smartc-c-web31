$(document).ready(function () {

    $('.progress').css('visibility', 'hidden').fadeOut();

    $('#modalPay').modal({
        dismissible: false,
        complete: function () {
            $('#amount').val('');
            $('#password_pay').val('');
            $('.password_pay-error').text('');
            $('.amount_pay-error').text('');
        }
    });
    
    socket.on('sell.pay.response', function (data) {
        updatePercentage(data.user);
        if(data.user != $('.email').text()){
            Materialize.toast('El usuario '+ data.user +' ha abonado: '+data.amount, 8000,'light-green darken-2');            
        }
    });
    
    socket.on('sell.pay_complete.response', function () {
        $('#btnPay').fadeOut();
        updateOwner();
        Materialize.toast('El producto ha sido vendido', 8000,'light-green darken-2');        
    });

    $(document).on('click', '#btnPay', function (evt) {
        evt.preventDefault();
        $('#modalPay').modal('open');
    });

    $(document).on('click', '#btn-sendPay', function (evt) {
        $('.progress').css('visibility', 'visible').fadeIn();
        evt.preventDefault();
        $.ajax({
            url: '/contracts/payment',
            type: 'POST',
            data: $('#form-payment').serialize(),
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden').fadeOut();
                if (data.status) {
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#modalPay').modal('close');
                    socket.emit('sell.pay',{'user':$('.email').text(),'amount':$('#amount').val()});
                } else {
                    Materialize.toast(data.result, 4000, 'rounded red');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden').fadeOut();
                Materialize.toast('Algo salió mal, intenta de nuevo', 4000, 'rounded red');
                $('#modalPay').modal('close');
            }
        });
    });
});

function updatePercentage(user) {
    $('.percentage-result').fadeOut(function () {
        $('.percentage-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/percentage',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.percentage-result').fadeOut(function () {
                $('.percentage-result').text(data.result).addClass('black-text').fadeIn();
            });

            if (data.result >= 100) {
                if(user == $('.email').text()){
                    socket.emit('sell.pay_complete');                
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.percentage-result').fadeOut(function () {
                $('.percentage-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}

function updateOwner() {
    $('.owner-result').fadeOut(function () {
        $('.owner-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/owner',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.owner-result').fadeOut(function () {
                $('.owner-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.owner-result').fadeOut(function () {
                $('.owner-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}
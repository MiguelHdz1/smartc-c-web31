$(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.modal').modal({
        dismissible: false
    });
    $('.tooltipped').tooltip({delay: 50});
    Materialize.toast($('.message-hidden').attr('data-bind'), 4000, 'rounded '+$('.message-hidden').attr('id'));
});
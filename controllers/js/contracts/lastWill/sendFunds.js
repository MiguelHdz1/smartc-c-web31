$(document).ready(function () {
    $('#progress-fund').css('visibility', 'hidden');

    $('#modal-fund').modal({
        dismissible: false,
        complete: function () {
            $('#amount').val('');
            $('#password-amount').val('');
            $('#progress-fund').css('visibility', 'hidden');
        }
    });
    $(document).on('click', '#btn-fund', function (evt) {
        evt.preventDefault();
        $('#modal-fund').modal('open');
    });
    
    socket.on('lastWill.funds.response', function (data) {
        updateFunds();
        updateLastTouch();
        if(data != $('.email').text()){
            Materialize.toast('El testamentario ha realizado un deposito', 8000,'light-green darken-2');
        }
    });    
    
    $(document).on('click', '#btn-fund-send', function (evt) {
        evt.preventDefault();
        $('#progress-fund').css('visibility', 'visible');
        $('#btn-fund-send').attr('disabled', true);
        var xhr = $.ajax({
            url: '/contracts/last-will/fund/participant',
            type: 'POST',
            data: {'password': $('#password-amount').val(), 'amount': $('#amount').val(), 'paramsId': $('#id-contract').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('#progress-fund').css('visibility', 'hidden');
                $('#btn-fund-send').attr('disabled', false);
                if(data.status){
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#modal-fund').modal('close');
                    socket.emit('lastWill.funds',$('.email').text());
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');
                }                                                                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#progress-fund').css('visibility', 'hidden');
                $('#btn-fund-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#modal-fund').modal('close');
            }
        });

        $(document).on('click', '#btn-fund-cancel', function (evt) {
            evt.preventDefault();
            $('#progress-fund').css('visibility', 'hidden');
            $('#btn-fund-send').attr('disabled', false);
            try {
                xhr.abort();
            } catch (err) {
            }
        });
    });
});

function updateFunds() {
    $('.amount-result').fadeOut(function () {
        $('.amount-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/last-will/amount/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.amount-result').fadeOut(function () {
                $('.amount-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.amount-result').fadeOut(function () {
                $('.amount-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}

function updateLastTouch(){
    $('.touch-result').fadeOut(function () {
        $('.touch-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/last-will/last-touch/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.touch-result').fadeOut(function () {
                $('.touch-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.amount-result').fadeOut(function () {
                $('.amount-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}

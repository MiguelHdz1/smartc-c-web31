$(document).ready(function(){
    $('.progress').css('visibility', 'hidden');
    
    $('#life-confirm').modal({
        dismissible: false,
        complete: function(){
            $('#password-confirm').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click','#btn-life',function(evt){
        evt.preventDefault();
        $('#life-confirm').modal('open');
    });
    
    socket.on('lastWill.alive.response', function (data) {
        updateLastTouch();
        if(data != $('.email').text()){
            Materialize.toast('El testamentario sigue vivo', 8000,'light-green darken-2');
        }
    });
    
    
    $(document).on('click','#btn-life-send',function(evt){
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-life-send').attr('disabled', true);
        var xhr = $.ajax({
            url:'/contracts/last-will/still-alive/participant',
            type: 'POST',
            data:{'password':$('#password-confirm').val(),'paramsId': $('#id-contract').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-life-send').attr('disabled', false);
                if(data.status){
                    socket.emit('lastWill.alive',$('.email').text());
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#life-confirm').modal('close');                                
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');
                }                                                               
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-life-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#life-confirm').modal('close');
            }
        });
        
        $(document).on('click','#btn-life-cancel',function(evt){
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-life-send').attr('disabled', false);
            try{
                xhr.abort();
            }catch(err){}            
        });
    });
});

function updateLastTouch() {
    $('.touch-result').fadeOut(function () {
        $('.touch-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/last-will/last-touch/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.touch-result').fadeOut(function () {
                $('.touch-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.touch-result').fadeOut(function () {
                $('.touch-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}
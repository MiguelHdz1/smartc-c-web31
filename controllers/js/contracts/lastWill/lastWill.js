$(document).ready(function () {
    $('.progress').css('visibility', 'hidden').fadeOut();
    var counter = 0;
    $(document).on('click', '#add-counter', function (evt) {
        evt.preventDefault();
        if (counter <= 3) {
            var addField = '<tr class="tr-section-'+(counter+2)+'"><td colspan="2"><div class="row"><div class="input-field col l8 m8 s12 offset-l2"><input id="counter' + (counter + 2) + '" class="isInput" name="counter[]" type="text"><label for="counter' + (counter + 2) + '">Direcci&oacute;n wallet ' + (counter + 2) + ' </label><div><span class="red-text counter' + (counter + 2) + '-error error"></span></div></div></div></td></tr>';
            $('#tr-main').append(addField);
            $('#tr-main').hide();
            $('#tr-main').fadeIn();            
            counter++;
        } else {
            Materialize.toast('Haz alcanzado el limite de contrapartes', 4000, 'rounded red');
        }
    });

    
    $(document).on('click', '#drop-counter', function (evt) {
        evt.preventDefault();
        if (counter > 0) {            
            $('.tr-section-'+(counter+1)).remove();
            counter--;
        }
    });        
});
$(document).ready(function () {
    $('.progress').css('visibility', 'hidden');

    $('#sync-modal').modal({
        dismissible: false,
        complete: function () {
            $('#password-sync').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click', '#btn-verify', function (evt) {
        evt.preventDefault();
        $('#sync-modal').modal('open');
    });
    
    socket.on('lastWill.sync.response', function (data) {
        updateState();
        updateFunds();
        if(data != $('.email').text()){
            Materialize.toast('Se ha sincronizado la situación del testamentario', 8000,'light-green darken-2');
        }
    });        

    $(document).on('click', '#btn-sync-send', function (evt) {
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-sync-send').attr('disabled', true);
        var xhr = $.ajax({
            url: '/contracts/last-will/sync/participant',
            type: 'POST',
            data: {'password': $('#password-sync').val(), 'paramsId': $('#id-contract').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-sync-send').attr('disabled', false);
                $('#sync-modal').modal('close');
                if(data.status){
                    Materialize.toast(data.result, 4000, 'rounded blue');                    
                    socket.emit('lastWill.sync',$('.email').text());  
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');
                }                                                                                              
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-sync-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#sync-modal').modal('close');
            }
        });

        $(document).on('click', '#btn-sync-cancel', function (evt) {
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-sync-send').attr('disabled', false);
            try {
                xhr.abort();
            } catch (err) {
            }
        });
    });
});

function updateState() {
    $('.status-result').fadeOut(function () {
        $('.status-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/last-will/status/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.result != 'Vivo') {
                $('#btn-verify').fadeOut();                
                Materialize.toast('El testamentario ha muerto', 8000,'light-green darken-2');        
            }
            $('.status-result').fadeOut(function () {
                $('.status-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.status-result').fadeOut(function () {
                $('.status-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}

function updateFunds() {
    $('.amount-result').fadeOut(function () {
        $('.amount-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/last-will/amount/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.amount-result').fadeOut(function () {
                $('.amount-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.amount-result').fadeOut(function () {
                $('.amount-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}
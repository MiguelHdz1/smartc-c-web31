$(document).ready(function () {

    $('#modal-password').modal({
        dismissible: false,
        complete: function () {
            $('#password_main').val('');
        }
    });

    $(document).on('click', '#create_contract', function (evt) {
        evt.preventDefault();
        $('#modal-password').modal('open');
    });

    $(document).on('click', '#ready_contract', function (evt) {
        evt.preventDefault();
        $('.progress').css('visibility', 'visible').fadeIn();
        $('#create_contract').attr('disabled', true);
        //$('#modal-password').modal('close');
        
        if ($('#smart-form').valid()) {
            var xhr = $.ajax({
                url: '/manage-contracts/last-will/deploy/v2',
                type: 'POST',
                data: $('#smart-form').serialize(),
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $('.progress').css('visibility', 'hidden');
                    $('#create_contract').attr('disabled', false);
                    if (data.status) {
                        Materialize.toast(data.message, 4000, 'rounded blue');
                        $('#modal').modal('close');
                        window.setTimeout(function(){window.location='/';},2000);     
                    } else {
                        Materialize.toast(data.message, 4000, 'rounded red');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    Materialize.toast('Ocurrio un error intente de nuevo', 4000, 'rounded red');
                    $('#create_contract').attr('disabled', false);
                    $('.progress').css('visibility', 'hidden');
                }
            });
            
            $(document).on('click','#cancel-request',function(evt){
                evt.preventDefault();
                $('.progress').css('visibility', 'hidden');
                $('#create_contract').attr('disabled', false);
                try{
                    xhr.abort();
                    window.location.href='/';
                }catch(err){
                    window.location.href='/';
                }
            });
        } else {
            //alert('closed');
            $('#modal-password').modal('close');
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
            $('#create_contract').attr('disabled', false);
            $('.progress').css('visibility', 'hidden');
        }                
    });
});
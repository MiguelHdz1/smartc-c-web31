$(document).ready(function () {
    $('.progress').css('visibility', 'hidden').fadeOut();
    var counter = 0;
    $(document).on('click', '#add-counter', function (evt) {
        evt.preventDefault();
        if (counter <= 3) {
            var addField='\
                    <tr class="tr-sect-'+(counter+2)+'">\n\
                        <td colspan="2">\n\
                            <div class="input-field">\n\
                                <input id="counter'+(counter+2)+'" name="counter[]" type="text" class="validate">\n\
                                <label for="counter'+(counter+2)+'">Dirección wallet '+(counter+2)+'</label>\n\
                            </div>\n\
                            <div class="col s12 center-align">\n\
                                <span class="red-text counter'+(counter+2)+'-error error"></span>\n\
                            </div>\n\
                        Porcentaje\n\
                            <p class="range-field">\n\
                                <input value="0" class="range-per" type="range" name="percentage[]" id="percentage'+(counter+2)+'" min="0" max="100" />\n\
                            </p>\n\
                            <div class="col s12 center-align">\n\
                                <span class="red-text percentage'+(counter+2)+'-error error"></span>\n\
                            </div>\n\
                        </td>\n\
                    </tr>';
            $('#counterparties-tbl tr:last').fadeIn(function(){
                $('#counterparties-tbl tr:last').after(addField);
            });            
            $('#tr-main').hide();
            $('#tr-main').fadeIn();            
            counter++;
        } else {
            Materialize.toast('Haz alcanzado el limite de contrapartes', 4000, 'rounded red');
        }
    });

    
    $(document).on('click', '#drop-counter', function (evt) {
        evt.preventDefault();
        if (counter > 0) {            
            $('.tr-sect-'+(counter+1)).remove();
            counter--;
            updatePercentage();
        }
    });        
    
    $(document).on('input','.range-per',function(){
        updatePercentage();
    });
    
    function updatePercentage(){
        let total = 0;
        $('input[name="percentage[]"').each(function(){
            total+=Number($(this).val());
        });
        $('#total-percent-in').val(total);
        $('#total-percent').html(total);
        
        if($('#total-percent-in').val() > 100){
            $('#total-percent').removeClass('black-text');
            $('#total-percent').addClass('red-text');
            $('#total-span').removeClass('black-text');
            $('#total-span').addClass('red-text');
        }else{
            $('#total-span').removeClass('red-text');
            $('#total-span').addClass('black-text');            
            $('#total-percent').removeClass('red-text');
            $('#total-percent').addClass('black-text');            
        }
    }
});
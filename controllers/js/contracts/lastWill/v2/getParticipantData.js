$(document).ready(function () {
    $('.collapsible').collapsible('open', 0);
    $('.collapsible').collapsible('open', 1);
    $('.collapsible').collapsible('open', 2);
    $('.collapsible').collapsible('open', 3);
    $('.collapsible').collapsible('open', 4);
    $('.collapsible').collapsible('open', 5);    
    var xhr;
    $('#collapsible-contract').collapsible({
        accordion: false,
        onOpen: function (el) {
            var action = $(el).attr('data-bind');
            var route = '';
            var classId = '';
            switch (action) {
                case '0':
                    route = '/contracts/last-will/v2/owner/participant';
                    classId = 'owner';
                    break;
                case '1':
                    route = '/contracts/last-will/v2/conditions/participant';
                    classId = 'conditions';
                    break;                
                case '2':
                    route = '/contracts/last-will/v2/amount/participant';
                    classId = 'amount';
                    break;
                case '3':
                    route = '/contracts/last-will/v2/status/participant';
                    classId = 'status';
                    break;
                case '4':
                    route = '/contracts/last-will/v2/last-touch/participant';
                    classId = 'touch';
                    break;
                case '5':
                    route = '/contracts/last-will/v2/counter/participant';
                    classId = 'counter';
                    break;
            }
            $('.' + classId + '-result').fadeOut(function () {
                $('.' + classId + '-result').html(preloader).fadeIn();
            });

            xhr = $.ajax({
                url: route,
                type: 'POST',
                data: {'paramsId': $('#id-contract').val()},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.status) {
                        if (Array.isArray(data.result)) {
                            var res = '<ol>';
                            var resMobile = '<ol>';
                            $.each(data.result, function (key, value) {
                                res += '<li>' + value +'   =>   '+data.percentages[key] +'% </li>';
                                resMobile += '<li>' + value.substr(0, value.length - 38) + ' ... ' + value.substr(value.length - 5, value)+'   ->   ' +data.percentages[key] +'% </li>';
                            });
                            res += '</ol>';
                            resMobile += '</ol>';
                            $('.' + classId + '-result').fadeOut(function () {
                                $('.' + classId + '-result').html(res).addClass('black-text').fadeIn();
                                $('.' + classId + '-result-mob').addClass('black-text').html(resMobile).fadeIn();
                            });                            
                        } else {
                            if(classId == 'status'){
                                if(data.result != 'Vivo'){                                    
                                    $('#btn-verify').fadeOut();
                                }
                            }                            
                            $('.' + classId + '-result').fadeOut(function () {
                                $('.' + classId + '-result').text(data.result).addClass('black-text').fadeIn();
                            });
                        }
                    } else {
                        $('.' + classId + '-result').fadeIn(function () {
                            $('.' + classId + '-result').html(data.result+' haciendo click en la cabezera').addClass('red-text');                            
                        });                        
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {                    
                    $('.' + classId + '-result').html('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text');
                    try{
                        xhr.abort();
                    }catch(err){}
                }
            });
        },
        onClose: function (el) {
            try {
                xhr.abort();
            } catch (err) {
            }
        }
    });
});
$('.progress').css('visibility', 'hidden');
$(document).ready(function () {
    $('#modal-password').modal({
        dismissible: false,
        complete: function () {
            $('#password_main').val('');
        }
    });


    $(document).on('click', '#create_contract', function (evt) {
        evt.preventDefault();
        $('#modal-password').modal('open');
    });

    $(document).on('click', '#ready_contract', function (evt) {
        evt.preventDefault();
        if ($('#smart-form').valid()) {
            $('#modal-password').modal('close');
            $('.progress').css('visibility', 'visible');
            //$('.isInput').attr('disabled', true);
            $('#create_contract').attr('disabled', true);
            var xhr = $.ajax({
                url: '/deploy-contract',
                type: 'POST',
                data: $('#smart-form').serialize(),
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $('.progress').css('visibility', 'hidden');
                    //$('.isInput').attr('disabled', false);
                    $('#create_contract').attr('disabled', false);
                    if (data.status) {
                        Materialize.toast(data.message, 4000, 'rounded blue');
                        window.setTimeout(function(){window.location='/';},2000);     
                    } else {
                        Materialize.toast(data.message, 4000, 'rounded red');
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    $('.progress').css('visibility', 'hidden');
                    //$('.isInput').attr('disabled', false);
                    $('#create_contract').attr('disabled', false);
                    Materialize.toast('Ha ocurrido un error, verifica e intenta de nuevo', 4000, 'rounded red');
                }
            });

        } else {
            Materialize.toast('Corregir todos los campos', 4000, 'rounded red');
            $('#modal-password').modal('close');
        }
        
        $(document).on('click', '#cancel-request', function (evt) {
            try{
                evt.preventDefault();
                xhr.abort();
                $('.progress').css('visibility', 'hidden');
                //$('.isInput').attr('disabled', false);
                $('#create_contract').attr('disabled', false);
                window.location.href='/';
            }                
            catch(err){
                window.location.href='/';
            }
            });
    });
});

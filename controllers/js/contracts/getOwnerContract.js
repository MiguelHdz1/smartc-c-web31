$(document).ready(function () {
    $('.collapsible').collapsible('open', 0);
    $('.collapsible').collapsible('open', 1);
    $('.collapsible').collapsible('open', 2);
    $('.collapsible').collapsible('open', 3);
    $('.collapsible').collapsible('open', 4);
    $('.collapsible').collapsible('open', 5);
    $('.collapsible').collapsible('open', 6);
    var xhr;
    $('#collapsible-contract').collapsible({
        accordion: false,
        onOpen: function (el) {
            var action = $(el).attr('data-bind');
            var route = '';
            var classId = '';
            switch (action) {
                case '0':
                    route = '/contracts/belong/owner';
                    classId = 'owner';
                    break;
                case '1':
                    route = '/contracts/belong/conditions';
                    classId = 'conditions';
                    break;
                case '2':
                    route = '/contracts/belong/product';
                    classId = 'product';
                    break;
                case '3':
                    route = '/contracts/belong/description';
                    classId = 'description';
                    break;
                case '4':
                    route = '/contracts/belong/price';
                    classId = 'price';
                    break;
                case '5':
                    route = '/contracts/belong/counter';
                    classId = 'counter';
                    break;
                case '6':
                    route = '/contracts/belong/percentage';
                    classId = 'percentage';
                    break;
            }
            $('.' + classId + '-result').fadeOut(function () {
                $('.' + classId + '-result').html(preloader).fadeIn();
            });

            xhr = $.ajax({
                url: route,
                type: 'POST',
                data: {'paramsId': $('#id-contract').val()},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.status) {
                        if (classId == 'counter') {
                            var res = '<ol>';
                            var resMobile = '<ol>';
                            $.each(data.result, function (key, value) {
                                res += '<li>' + value + '</li>';
                                resMobile += '<li>' + value.substr(0, value.length - 38) + ' ... ' + value.substr(value.length - 5, value) + '</li>';
                            });
                            res += '</ol>';
                            resMobile += '</ol>';
                            $('.' + classId + '-result').fadeOut(function () {
                                $('.' + classId + '-result').html(res).addClass('black-text').fadeIn();
                                $('.' + classId + '-result-mob').html(resMobile).addClass('black-text').fadeIn();
                            });
                        } else {
                            $('.' + classId + '-result').fadeOut(function () {
                                $('.' + classId + '-result').text(data.result).addClass('black-text').fadeIn();
                            });
                        }
                    } else {
                        $('.' + classId + '-result').fadeIn(function () {
                            $('.' + classId + '-result').text(data.result + ' haciendo click en la cabezera').addClass('red-text');
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.' + classId + '-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text');
                    try {
                        xhr.abort();
                    } catch (err) {
                    }
                }
            });
        },
        onClose: function (el) {
            try {
                xhr.abort();
            } catch (err) {
            }
        }
    });
});
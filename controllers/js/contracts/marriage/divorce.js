$(document).ready(function(){
    $('.progress').css('visibility', 'hidden');
    
    $('#divorce-modal').modal({
        dismissible: false,
        complete: function(){
            $('#password-divorce').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click','#btn-divorce-answer',function(evt){
        evt.preventDefault();
        $('#divorce-modal').modal('open');
    });
    
    socket.on('marriage.divorce_answer.response', function (data) {
        if(data != $('.email').text()){
            var $toastContent = $('<span>Se ha respondido solicitud de divoricio, recarga la página o da clic aqui</span>').add($('<button id ="reload" class="btn-flat toast-action"><i class="material-icons">replay</i></button>'));
            Materialize.toast($toastContent, 8000,'light-green darken-2');            
        }        
    });
    
    $(document).on('click','#btn-divorce-send',function(evt){
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-divorce-send').attr('disabled', true);
        var xhr = $.ajax({
            url:'/contracts/marriage/divorce-answer',
            type: 'POST',
            data:{'password':$('#password-divorce').val(),'paramsId': $('#id-contract').val(),'answer': $('input[name=answer-divorce]:checked').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-send').attr('disabled', false);
                if(data.status){                    
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#divorce-modal').modal('close');
                    socket.emit('marriage.divorce_answer',$('.email').text());
                    window.setTimeout(location.reload(),2000);
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');               
                    $('#divorce-modal').modal('close');
                }                                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#divorce-modal').modal('close');
            }
        });
        
        $(document).on('click','#btn-divorce-cancel',function(evt){
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-divorce-send').attr('disabled', false);
            try{
                xhr.abort();
            }catch(err){}            
        });
    });
});
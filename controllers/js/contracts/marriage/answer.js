$(document).ready(function(){
    $('.progress').css('visibility', 'hidden');
    
    $('#answer-modal').modal({
        dismissible: false,
        complete: function(){
            $('#password-confirm').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click','#btn-answer',function(evt){
        evt.preventDefault();
        $('#answer-modal').modal('open');
    });
    
    socket.on('marriage.answer.response', function (data) {
        if(data != $('.email').text()){
            var $toastContent = $('<span>La solicitud ha sido respondida, recarga la página o da clic aqui</span>').add($('<button id ="reload" class="btn-flat toast-action"><i class="material-icons">replay</i></button>'));
            Materialize.toast($toastContent, 8000,'light-green darken-2');            
        }
    });
    
    $(document).on('click','#btn-answer-send',function(evt){
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-answer-send').attr('disabled', true);
        var xhr = $.ajax({
            url:'/contracts/marriage/answer',
            type: 'POST',
            data:{'password':$('#password-confirm').val(),'paramsId': $('#id-contract').val(),'answer': $('input[name=answer-user]:checked').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-answer-send').attr('disabled', false);
                if(data.status){
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#answer-modal').modal('close');
                    socket.emit('marriage.answer',$('.email').text());
                    window.setTimeout(location.reload(),2000);
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');               
                    $('#answer-modal').modal('close');
                }                                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-answer-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#answer-modal').modal('close');
            }
        });
        
        $(document).on('click','#btn-answer-cancel',function(evt){
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-answer-send').attr('disabled', false);
            try{
                xhr.abort();
            }catch(err){}            
        });
    });
});

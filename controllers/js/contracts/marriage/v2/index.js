$(document).ready(function(){    
    $('select').material_select();
    $('#modal-password').modal({
        dismissible:false,
        complete: function(){
            $('#password_main').val('');
        }
    });
    
    $(document).on('click','#create_contract',function(evt){
        evt.preventDefault();
        $('#modal-password').modal('open');
    });
});
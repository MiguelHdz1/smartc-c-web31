$(document).ready(function () {
    $('#modal-deposit').modal({
        complete: function () {
            $('#amount').val('');
            $('#password-deposit').val('');
            $('.amount-error').text('');
            $('.password-deposit-error').text('');
        }
    });
    var xhr;
    
    socket.on('marriage.funds.response', function (data) {
        updateFunds();
        if(data.user != $('.email').text()){
            Materialize.toast(data.user +' ha depositado '+ data.amount, 4000, 'light-green darken-2');
        }
        
    });

    $(document).on('click','#btn-deposit', function (evt) {
        evt.preventDefault();
        $('#modal-deposit').modal('open');
    });

    $(document).on('click', '#btn-deposit-send', function (evt) {
        evt.preventDefault();        
        if ($('#form-deposit').valid()) {
            $('#btn-deposit-send').attr('disabled', true);
            $('.progress').css('visibility', 'visible');
            xhr =$.ajax({
                url: '/contracts/marriage/deposit',
                type: 'POST',
                data: {'password':$('#password-deposit').val(),'amount': $('#amount').val(),'paramsId': $('#id-contract').val()},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $('.progress').css('visibility', 'hidden');
                    $('#btn-deposit-send').attr('disabled', false);
                    if (data.status) {
                        Materialize.toast(data.result, 4000, 'rounded blue');
                        $('#modal-deposit').modal('close');
                        socket.emit('marriage.funds',{'user':$('.email').text(),'amount':$('#amount').val()});                        
                    } else {
                        Materialize.toast(data.result, 4000, 'rounded red');
                        $('#modal-deposit').modal('close');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.progress').css('visibility', 'hidden');
                    $('#btn-deposit-send').attr('disabled', false);
                }
            });
            
            $(document).on('click','#btn-deposit-cancel',function(evt){
                evt.preventDefault();
                try{
                    xhr.abort();
                    $('#modal-deposit').modal('close');
                }catch(err){
                    $('#modal-deposit').modal('close');
                }                
            });
        } else {
            Materialize.toast('Corregir todos los campos.', 4000, 'rounded red');
        }
    });
});

function updateFunds() {
    $('.balance-result').fadeOut(function () {
        $('.balance-result').html(preloader).fadeIn();
    });
    $.ajax({
        url: '/contracts/marriage/balance/participant',
        type: 'POST',
        data: {'paramsId': $('#id-contract').val()},
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            $('.balance-result').fadeOut(function () {
                $('.balance-result').text(data.result).addClass('black-text').fadeIn();
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('.balance-result').fadeOut(function () {
                $('.balance-result').text('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text').fadeIn();
            });
        }
    });
}
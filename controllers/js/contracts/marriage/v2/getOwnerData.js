$(document).ready(function () {
    
    $('.collapsible').collapsible('open', 0);
    $('.collapsible').collapsible('open', 1);
    $('.collapsible').collapsible('open', 2);
    $('.collapsible').collapsible('open', 3);
    $('.collapsible').collapsible('open', 4);
    $('.collapsible').collapsible('open', 5);
    $('.collapsible').collapsible('open', 6);
    $('.collapsible').collapsible('open', 7);
    var xhr;
    $('#collapsible-contract').collapsible({
        accordion: false,
        onOpen: function (el) {
            var action = $(el).attr('data-bind');
            var route = '';
            var classId = '';
            switch (action) {
                case '0':
                    route = '/contracts/marriage/proposer/owner';
                    classId = 'proposer';
                    break;
                case '1':
                    route = '/contracts/marriage/proposed/owner';
                    classId = 'proposed';
                    break;
                case '2':
                    route = '/contracts/marriage/conditions/owner';
                    classId = 'conditions';
                    break;
                case '3':
                    route = '/contracts/marriage/proposed-date/owner';
                    classId = 'proposed-date';
                    break;
                case '4':
                    route = '/contracts/marriage/answer-date/owner';
                    classId = 'answer-date';
                    break;
                case '5':
                    route = '/contracts/marriage/balance/owner';
                    classId = 'balance';
                    break;
                case '6':
                    route = '/contracts/marriage/regimen/owner';
                    classId = 'regimen';
                    break;
                case '7':
                    route = '/contracts/marriage/divorce/owner';
                    classId = 'divorce';
                    break;
            }
            $('.' + classId + '-result').fadeOut(function () {
                $('.' + classId + '-result').html(preloader).fadeIn();
            });

            xhr = $.ajax({
                url: route,
                type: 'POST',
                data: {'paramsId': $('#id-contract').val()},
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.status) {
                        $('.' + classId + '-result').fadeOut(function () {
                            $('.' + classId + '-result').text(data.result).addClass('black-text').fadeIn();
                        });
                    } else {
                        $('.' + classId + '-result').fadeIn(function () {
                            $('.' + classId + '-result').html(data.result+' haciendo click en la cabezera').addClass('red-text');
                        });                        
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {                    
                    $('.' + classId + '-result').html('Ocurrió un error, intenta de nuevo haciendo click en la cabezera del elemento..').addClass('red-text');                    
                    try {
                        xhr.abort();
                    } catch (err) {
                    }
                }
            });
        },
        onClose: function (el) {
            try {
                xhr.abort();
            } catch (err) {
            }
        }
    });
});
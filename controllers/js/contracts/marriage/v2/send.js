$(document).ready(function () {
    $('.progress').css('visibility', 'hidden');
    
    $(document).on('click', '#ready_contract', function (evt) {
        evt.preventDefault();
        if ($('#smart-form').valid()) {
            $('.progress').css('visibility', 'visible');
            $('#ready_contract').attr('disabled',true);
            
            var xhr= $.ajax({
                url: '/contracts/marriage/v2/deploy',
                type: 'POST',
                data: $('#smart-form').serialize(),
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    $('#ready_contract').attr('disabled',false);
                    $('.progress').css('visibility', 'hidden');
                    $('#modal-password').modal('close');       
                    if (data.status) {
                        Materialize.toast(data.message, 4000, 'rounded blue');
                        window.setTimeout(function () {window.location = '/';}, 2000);
                    }else{
                        Materialize.toast(data.message, 4000, 'rounded red');
                    }                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.progress').css('visibility', 'hidden');
                    $('#ready_contract').attr('disabled',false);
                    $('#modal-password').modal('close');       
                    Materialize.toast('Ha ocurrido un error, verifica e intenta de nuevo', 4000, 'rounded red');
                }
            });
            
            $(document).on('click','#cancel-contract',function(evt){
                evt.preventDefault();
                $('.progress').css('visibility', 'hidden');
                $('#ready_contract').attr('disabled',false);
                try{
                    xhr.abort();
                    window.location.href='/';
                }catch(err){
                    window.location.href='/';
                }
            });
        } else {
            $('#modal-password').modal('close');        
            Materialize.toast('Corregir todos los campos.', 4000, 'rounded red');
        }
    });
});
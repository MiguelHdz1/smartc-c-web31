$(document).ready(function(){
    $('.progress').css('visibility', 'hidden');
    
    $('#divorce-modal-cancel').modal({
        dismissible: false,
        complete: function(){
            $('#password-divorce-cancel').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click','#btn-divorce-abort',function(evt){
        evt.preventDefault();
        $('#divorce-modal-cancel').modal('open');
    });
    
    socket.on('marriage.divorce_cancel.response', function (data) {
        if(data != $('.email').text()){
            var $toastContent = $('<span>Se ha cancelado solicitud de divoricio, recarga la página o da clic aqui</span>').add($('<button id ="reload" class="btn-flat toast-action"><i class="material-icons">replay</i></button>'));
            Materialize.toast($toastContent, 8000,'rounded light-green darken-2');            
        }        
    });
    
    $(document).on('click','#btn-divorce-clc-send',function(evt){
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-divorce-clc-send').attr('disabled', true);
        var xhr = $.ajax({
            url:'/contracts/marriage/divorce-cancel',
            type: 'POST',
            data:{'password':$('#password-divorce-cancel').val(),'paramsId': $('#id-contract').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-clc-send').attr('disabled', false);
                if(data.status){
                    socket.emit('marriage.divorce_cancel',$('.email').text());
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#divorce-modal-cancel').modal('close');
                    window.setTimeout(location.reload(),2000);
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');               
                    $('#divorce-modal-cancel').modal('close');
                }                                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-clc-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#divorce-modal-cancel').modal('close');
            }
        });
        
        $(document).on('click','#btn-divorce-cancel',function(evt){
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-divorce-clc-send').attr('disabled', false);
            try{
                xhr.abort();
            }catch(err){}            
        });
    });
});
$(document).ready(function(){
    $('.progress').css('visibility', 'hidden');
    
    $('#req-divorce-modal').modal({
        dismissible: false,
        complete: function(){
            $('#password-divorce-req').val('');
            $('.progress').css('visibility', 'hidden');
        }
    });
    $(document).on('click','#btn-divorce',function(evt){
        evt.preventDefault();
        $('#req-divorce-modal').modal('open');
    });
    
    socket.on('marriage.divorce_request.response', function (data) {
        if(data != $('.email').text()){
            var $toastContent = $('<span>Haz recibido una solicitud de divoricio, recarga la página o da clic aqui</span>').add($('<button id ="reload" class="btn-flat toast-action"><i class="material-icons">replay</i></button>'));
            Materialize.toast($toastContent, 8000,'light-green darken-2');            
        }        
    });
    
    $(document).on('click','#reload',function(evt){
        evt.preventDefault();
        location.reload();
    });
    
    $(document).on('click','#btn-divorce-req-send',function(evt){
        evt.preventDefault();
        $('.progress').css('visibility', 'visible');
        $('#btn-divorce-req-send').attr('disabled', true);
        var xhr = $.ajax({
            url:'/contracts/marriage/divorce-req',
            type: 'POST',
            data:{'password':$('#password-divorce-req').val(),'paramsId': $('#id-contract').val()},
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-req-send').attr('disabled', false);
                if(data.status){
                    Materialize.toast(data.result, 4000, 'rounded blue');
                    $('#req-divorce-modal').modal('close');
                    socket.emit('marriage.divorce_request',$('.email').text());
                    window.setTimeout(location.reload(),2000);
                }else{
                    Materialize.toast(data.result, 4000, 'rounded red');               
                    $('#req-divorce-modal').modal('close');
                }                                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.progress').css('visibility', 'hidden');
                $('#btn-divorce-req-send').attr('disabled', false);
                Materialize.toast('Ocurrió un error, intente de nuevo.', 4000, 'rounded red');
                $('#req-divorce-modal').modal('close');
            }
        });
        
        $(document).on('click','#btn-divorce-req-cancel',function(evt){
            evt.preventDefault();
            $('.progress').css('visibility', 'hidden');
            $('#btn-divorce-req-send').attr('disabled', false);
            try{
                xhr.abort();
            }catch(err){}            
        });
    });
});
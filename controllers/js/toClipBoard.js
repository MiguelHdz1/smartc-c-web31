$(document).ready(function(){
    $(document).on('click','#copy-to',function(evt){
        evt.preventDefault();
        var $temp = $('<input>');
        $('body').append($temp);
        $temp.val($('#copy-to').attr('data-bind')).select();
        document.execCommand("copy");
        $temp.remove();
        Materialize.toast('Dirección copiada al portapapeles', 4000, 'rounded blue');
    });
});
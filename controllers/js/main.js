$(document).ready(function () {
    Materialize.toast($('.message').text(), 3000, 'rounded ' + $('.message').attr('id'));
    $('#message').on('click', function (e) {
        e.preventDefault();
        $("#balance").fadeOut(function () {
            $('#balance').text('Espere...').fadeIn();
        });
        $('.tap-target').tapTarget('open');
        $.ajax({
            url: '/get-balance',
            type: 'GET',
            dataType: 'json',
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    $("#balance").fadeOut(function () {
                        $('#balance').text(data.result).fadeIn();
                    });

                } else {
                    $("#balance").fadeOut(function () {
                        $('#balance').text(data.result).fadeIn();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#balance").fadeOut(function () {
                    $('#balance').text('Ocurrio un error intente de nuevo.').fadeIn();
                });
            }

        });
    });

});

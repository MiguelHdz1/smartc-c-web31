$(document).ready(function () {
    $("#form-account").validate({
        ignore: "",
        rules: {
            name_user: {required: true, maxlength: 40, minlength: 3},
            first_name: {required: true, maxlength: 40, minlength: 3},
            last_name: {required: true, maxlength: 40, minlength: 3},
            email: {required: true, email: true},
            password_user: {required: true, maxlength: 15, minlength: 5},
            password_confirm: {required: true, equalTo: "#password_user"}
        },
        messages: {
            name_user: {
                required: 'Este campo es obligatorio',
                maxlength:'El valor no debe ser menor a 40 caracteres',
                minlength: 'El valor debe ser mayor a 3 caracteres'
            },
            first_name: {
                required: 'Este campo es obligatorio',
                maxlength:'El valor no debe ser menor a 40 caracteres',
                minlength: 'El valor debe ser mayor a 3 caracteres'
            },
            last_name: {
                required: 'Este campo es obligatorio',
                maxlength:'El valor no debe ser menor a 40 caracteres',
                minlength: 'El valor debe ser mayor a 3 caracteres'
            },
            email: {
                required: 'Este campo es obligatorio',
                email: 'No es un formato de correo valido'
            },
            password_user: {
                required: 'Este campo es obligatorio',
                maxlength:'El valor no debe ser menor a 15 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            password_confirm: {
                required: 'Este campo es obligatorio', 
                equalTo: 'La contraseña no coincide'
            }
        },
        errorPlacement: function (error, element) {
            $('.' + element.attr('name') + '-error').text(error.text());
        },
        success: function (error) {
            console.log('possible');
        }

    });
});
$(document).ready(function () {
    $("#form-payment").validate({
        ignore: "",
        rules: {
            amount: {
                required: true,
                number:true,
                min: 0.1
            },
            password_pay: {
                required: true
            }            
        },
        messages: {
            amount: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser un número',
                min: 'El valor minimo debe de ser de 0.1'
            },
            password_pay: {
                required: 'Campo requerido'
            }
        },
        errorPlacement: function (error, element) {
            
                //console.log(element.attr('name'));
                $('.' + element.attr('name') + '-error').text(error.text());                
            
        },
        success: function (error) {            
        }
    });
});
$(document).ready(function () {
    $("#smart-form").validate({
        ignore: "",
        rules: {
            contractName: {
                required: true,
                minlength: 5,
                maxlength: 50
            },
            proposer: {
                required: true,
                minlength: 42,
                maxlength: 42
            },
            proposed: {
                required: true,
                minlength: 42,
                maxlength: 42
            },
            conditions: {
                required: true,
                minlength: 10,
                maxlength: 500
            },
            marriage_regimen: {
                required: true
            }
        },
        messages: {
            contractName: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 50 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            proposed: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres'
            },
            proposer: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres'
            },
            marriage_regimen:{
                required: "Campo requerido"
            },
            conditions: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 500 caracteres',
                minlength: 'El valor debe ser mayor a 10 caracteres'}
        },
        errorPlacement: function (error, element) {
            
                //console.log(element.attr('name'));
                $('.' + element.attr('id') + '-error').text(error.text());                
            
        },
        success: function (error) {            
        }
    });
});
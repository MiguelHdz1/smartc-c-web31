$(document).ready(function () {
    $("#form-deposit").validate({
        ignore: "",
        rules: {
            'password-deposit': {
                required: true,
                minlength: 1
            },
            amount: {
                required: true,
                number: true,
                min: 0.1
            }
        },
        messages: {
            'password-deposit': {
                required: 'Campo requerido',
                minlength: 'El valor debe ser mayor a 1 caracteres'
            },
            amount: {
                required: 'Campo requerido',
                number: 'Ingresa un número valido',
                min: 'El precio debe ser mayor a 0.1'
            }
        },
        errorPlacement: function (error, element) {

            //console.log(element.attr('name'));
            $('.' + element.attr('id') + '-error').text(error.text());
        },
        success: function (error) {
        }
    });
});
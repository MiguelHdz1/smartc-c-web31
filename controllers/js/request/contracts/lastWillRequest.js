$(document).ready(function () {
    $("#smart-form").validate({
        ignore: "",
        rules: {
            contractName: {
                required: true,
                minlength: 5,
                maxlength: 50
            },
            ownerContract: {
                required: true,
                minlength: 42,
                maxlength: 42
            },
            'counter[]': {
                required: true,
                minlength: 42,
                notEqual :"#ownerContract",
                maxlength: 42
            },
            conditions: {
                required: true,
                minlength: 10,
                maxlength: 500
            }
        },
        messages: {
            contractName: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 50 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            ownerContract: {
                required: 'Campo requerido',
                minlength: 'El valor debe ser igual a 42 caracteres',                
                maxlength: 'El valor debe ser igual a 42 caracteres'
                     
            },
            'counter[]': {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres',
                notEqual: 'Establecer un valor diferente al del testamentario.'
            },
            conditions: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 500 caracteres',
                minlength: 'El valor debe ser mayor a 10 caracteres'}
        },
        errorPlacement: function (error, element) {
            
                //console.log(element.attr('name'));
                $('.' + element.attr('id') + '-error').text(error.text());                
            
        },
        success: function (error) {            
        }
    });
});
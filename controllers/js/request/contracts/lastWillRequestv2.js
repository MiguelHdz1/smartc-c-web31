$(document).ready(function () {
    $("#smart-form").validate({
        ignore: "",
        rules: {
            contractName: {
                required: true,
                minlength: 5,
                maxlength: 50
            },
            ownerContract: {
                required: true,
                minlength: 42,
                maxlength: 42
            },
            'counter[]': {
                required: true,
                minlength: 42,
                notEqual :"#ownerContract",
                maxlength: 42
            },            
            conditions: {
                required: true,
                minlength: 10,
                maxlength: 500
            },
            'percentage[]':{
                required: true,
                min:1,
                max: 100
            },
            "total-percent-in":{
                required: true,
                min: 100,
                max:100
            },
            'seconds':{
                required: true,
                min: 10,
                max: 300
            }
        },
        messages: {
            contractName: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 50 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            ownerContract: {
                required: 'Campo requerido',
                minlength: 'El valor debe ser igual a 42 caracteres',                
                maxlength: 'El valor debe ser igual a 42 caracteres'
                     
            },
            'counter[]': {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres',
                notEqual: 'Establecer un valor diferente al del testamentario.'
            },
            conditions: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 500 caracteres',
                minlength: 'El valor debe ser mayor a 10 caracteres'
            },
            'total-percent-in':{
                min: 'El porcentaje total debe ser igual a 100',
                max: 'El porcentaje total debe ser igual a 100',
                required: 'Campo requerido'
            },
            'percentage[]':{
                required: 'Campo requerido',
                min:'El valor minimo debe ser 1',
                max: 'El valor máximo debe ser 100'
            },
            'seconds':{
                required: 'Campo requerido',
                min: 'Debes de seleccionar al menos 10 segundos',
                max: 'Solo puedes seleccionar hasta 300 segundos'
            }
        },
        errorPlacement: function (error, element) {
            $('.' + element.attr('id') + '-error').text(error.text());            
        },
        success: function (error) {            
        }
    });
});
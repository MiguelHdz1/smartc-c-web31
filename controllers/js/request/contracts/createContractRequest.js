$(document).ready(function () {
    $("#smart-form").validate({
        ignore: "",
        rules: {
            contractName: {
                required: true,
                minlength: 5,
                maxlength: 50
            },
            ownerContract: {
                required: true,
                minlength: 42,
                maxlength: 42
            },
            'counter[]': {
                required: true,
                notEqual :"#ownerContract",
                minlength: 42,
                maxlength: 42
            },
            conditions: {
                required: true,
                minlength: 10,
                maxlength: 500
            },
            nameProduct: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            priceProduct: {
                required: true,
                number: true,
                min: 0.1
            },
            details: {
                required: true,
                minlength: 5
            },
            conditionsAcc: {
                required: true,
                minlength: 1,
                maxlength: 1
            }            
        },
        messages: {
            contractName: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 50 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            ownerContract: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres'
            },
            'counter[]': {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser igual a 42 caracteres',
                minlength: 'El valor debe ser igual a 42 caracteres',
                notEqual: 'Establecer un valor diferente al del dueño.'
            },
            conditions: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 500 caracteres',
                minlength: 'El valor debe ser mayor a 10 caracteres'},
            nameProduct: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 60 caracteres',
                minlength: 'El valor debe ser mayor a 3 caracteres'
            },
            priceProduct: {
                required: 'Campo requerido',
                number: 'Ingresa un número valido',
                min: 'El precio debe ser mayor a 0.1'
            },
            details: {
                required: 'Campo requerido',
                maxlength: 'El valor debe ser menor o igual a 500 caracteres',
                minlength: 'El valor debe ser mayor a 5 caracteres'
            },
            conditionsAcc: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            
                //console.log(element.attr('name'));
                $('.' + element.attr('id') + '-error').text(error.text());                
            
        },
        success: function (error) {            
        }
    });
});
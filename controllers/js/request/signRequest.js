$(document).ready(function () {
    $("#sign_form").validate({
        ignore: "",
        rules: {
            acc_sign: {required: true},
            password: {required: true}
        },
        messages: {           
            acc_sign: {required: 'Campo requerido'},
            password: {required: 'Campo requerido'}
        },
        errorPlacement: function (error, element) {
            $('.'+element.attr('name')+'-error').text(error.text());                      
        },
        success: function (error) {
            console.log('possible');
        }

    });
});